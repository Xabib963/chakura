import 'package:chakura/features/GetIn/Domain/UseCases/RegisterUseCase.dart';
import 'package:chakura/features/Order/Domain/UseCases/AddOrderUseCase.dart';
import 'package:chakura/features/Order/Domain/UseCases/GetOrdersUseCase.dart';
import 'package:chakura/features/Order/Domain/repositries/OrderREpositries.dart';
import 'package:chakura/features/Order/data/Data%20Sources/OrderRemoteData.dart';
import 'package:chakura/features/Order/data/repositries/OrderRepositryImple.dart';
import 'package:chakura/features/Order/presentation/Blocs/bloc/orders_bloc_bloc.dart';
import 'package:chakura/features/Products/Domain/usecases/AddToFvoritesUseCases.dart';
import 'package:chakura/features/Products/Domain/usecases/CheckstatusUsecase.dart';
import 'package:chakura/features/Products/Domain/usecases/DeletFromFavorites.dart';
import 'package:chakura/features/Products/Domain/usecases/GetFavoritesUsecase.dart';
import 'package:chakura/features/Products/Domain/usecases/GetUserProductsUseCase.dart';
import 'package:chakura/features/Products/Domain/usecases/GetfurProducts.dart';
import 'package:chakura/features/Products/Domain/usecases/GetgamesUSeCase.dart';

import 'package:chakura/features/Products/presentation/Page/Cart.dart';
import 'package:chakura/features/Profile/data/repositories/GetUserReposImplemintation.dart';
import 'package:chakura/features/Profile/domain/repositories/getUserRepos.dart';
import 'package:chakura/features/Profile/presintation/bloc/user_profile_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/Network/connection_info.dart';
import 'features/GetIn/Data/AuthRepos/AuthRepostriesImpl.dart';
import 'features/GetIn/Data/DataProvider/Auth.dart';
import 'features/GetIn/Domain/Reposteries/AuthRepostries.dart';
import 'features/GetIn/Domain/UseCases/LoginUsecase.dart';
import 'features/GetIn/presentaion/bloc/loginBloc/bloc/get_in_bloc.dart';
import 'features/Products/Domain/entities/CartProduct.dart';
import 'features/Products/Domain/entities/Product.dart';
import 'features/Products/Domain/repositries/ProductRepositry.dart';
import 'features/Products/Domain/usecases/AddNewProductsUSecase.dart';
import 'features/Products/Domain/usecases/DeleteProductUseCase.dart';
import 'features/Products/Domain/usecases/GetAllproductUseCase.dart';
import 'features/Products/Domain/usecases/GetCartProuductsUseCase.dart';
import 'features/Products/Domain/usecases/UpdateProductUseCase.dart';
import 'features/Products/data/DataProviders/LocalDB.dart';
import 'features/Products/data/DataProviders/ProductsDataSource.dart';
import 'features/Products/data/Repositries/repositry.dart';
import 'features/Products/presentation/Blocs/bloc/addto_hive_bloc.dart';
import 'features/Products/presentation/Blocs/bloc/get_add_up_delete_bloc.dart';
import 'features/Products/presentation/Cubit/chooseTypeCubit.dart/cubit/choose_type_cubit.dart';
import 'features/Profile/data/DataProviders/GetUserApi.dart';
import 'features/Profile/domain/UseCases/GetUserInfoUsecase.dart';

final GetIt dependecy = GetIt.instance;

Future<void> init() async {
//Bloc
// registerFactory we use it when we need to define more than one obj
  dependecy.registerFactory(() => GetAddUpDeleteBloc(
      deleteProductUseCase: dependecy(),
      getUserProducts: dependecy(),
      getfurProductUseCase: dependecy(),
      addNewProductsUSecase: dependecy(),
      getAllproductUseCase: dependecy(),
      updateProductUseCase: dependecy()));
  dependecy.registerFactory(() => GetInBloc(dependecy(), dependecy()));
  dependecy.registerFactory(() => ChooseTypeCubit());
  dependecy.registerFactory(() => AddtoHiveBloc(
      dependecy(), dependecy(), dependecy(), dependecy(), dependecy()));
  dependecy.registerFactory(() => OrdersBloc(dependecy(), dependecy()));
  dependecy.registerFactory(() => UserProfileBloc(dependecy()));
//usecases
// registerSingleton we use it when we need to define only one obj
  dependecy
      .registerLazySingleton(() => GetElectronicsProductUseCase(dependecy()));
  dependecy.registerLazySingleton(() => GetfurProductUseCase(dependecy()));
  dependecy.registerLazySingleton(() => GetgamesProductUseCase(dependecy()));
  dependecy.registerLazySingleton(() => UpdateProductUseCase(dependecy()));
  dependecy.registerLazySingleton(() => DeleteProductUseCase(dependecy()));
  dependecy.registerLazySingleton(() => AddNewProductsUSecase(dependecy()));
  dependecy.registerLazySingleton(() => AddToFvoritesUseCases(dependecy()));
  dependecy.registerLazySingleton(() => CheckStatusUseCase(dependecy()));
  dependecy.registerLazySingleton(() => GetFavoritesItemsUseCase(dependecy()));
  dependecy.registerLazySingleton(() => GetCartsItemsUseCase(dependecy()));
  dependecy.registerLazySingleton(() => Remove_Product_FromLocal(dependecy()));
  dependecy.registerLazySingleton(() => AddOrderUsecase(
        orderRepositry: dependecy(),
      ));
  dependecy.registerLazySingleton(() => GetProfileInfoUseCases(
        dependecy(),
      ));
  dependecy.registerLazySingleton(() => GetUserProductsUseCase(dependecy()));
  dependecy.registerLazySingleton(() => LoginUseCase(dependecy()));
  dependecy.registerLazySingleton(() => GetOrdersUseCase(
        orderRepositry: dependecy(),
      ));
  dependecy.registerLazySingleton(() => RegisterUseCase(dependecy()));
//repository
  dependecy.registerLazySingleton<ProductsRepositry>(() => ProductRepositryImpl(
        remoteDataSource: dependecy(),
        localDB: dependecy(),
        networkInfo: dependecy(),
      ));
  dependecy.registerLazySingleton<GetUserRepos>(() => GetUserReposImplimetation(
        dependecy(),
        networkInfo: dependecy(),
      ));
  dependecy.registerLazySingleton<OrderRepositry>(() => OrderRepositryImple(
        networkInfo: dependecy(),
        orderRemoteDataSourceImplemintation: dependecy(),
      ));
  dependecy.registerLazySingleton<AuthRepostries>(() => AuthRepositriesImple(
        dependecy(),
        dependecy(),
      ));
//data Sources
  dependecy.registerLazySingleton<ProductREmoteDataSource>(
      () => RemoteDataWithHttp());

  dependecy.registerLazySingleton<LocalDB>(() => LocalDB());
  dependecy.registerLazySingleton<OrderRemoteDataSourceImplemintation>(
      () => OrderRemoteDataSourceImplemintation());
  dependecy.registerLazySingleton(() => AuthProvider(dependecy()));
  dependecy.registerLazySingleton(() => GetUserApi());
//core
  dependecy
      .registerLazySingleton<NetworkInfo>(() => NetworkInfoImp(dependecy()));

//ex
  await Hive.initFlutter();
  Hive.registerAdapter(ProductAdapter());
  Hive.registerAdapter(CartProductAdapter());
  await LocalDB.instance.openFavoritesBox();
  await LocalDB.instance.opencartsBox();
  final sharedPreferences = await SharedPreferences.getInstance();
  // dependecy.registerLazySingleton(() => favoritesBox);
  // // dependecy.registerLazySingleton(() => carts);
  dependecy.registerLazySingleton(() => sharedPreferences);
  dependecy.registerLazySingleton(() => InternetConnectionChecker());
}
