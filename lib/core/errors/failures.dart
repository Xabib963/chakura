import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {}

class OffLineFailure extends Failure {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class ServicesFailure extends Failure {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class DataSourceFailure extends Failure {
  @override
 
  List<Object?> get props => [];
}

class NOTAUTHEDFailure extends Failure {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}
