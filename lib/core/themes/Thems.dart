import 'package:flutter/material.dart';

class ColorTheme {
  static Color main = const Color.fromRGBO(19, 18, 52, 1);
  static Color mainOpa = Color.fromRGBO(118, 223, 165, 1);
  static Color mainLight = Color.fromRGBO(46, 42, 153, 1);
  static Color secondary = Color.fromRGBO(38, 34, 113, 1);
  static Color additional = Color.fromRGBO(255, 154, 254, 1);
  static Color additional2 = Color.fromRGBO(249, 225, 106, 1);
}

TextStyle textStyle(
    {required double size, required Color color, required FontWeight weight}) {
  return TextStyle(color: color, fontSize: size, fontWeight: weight);
}
