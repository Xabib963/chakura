import 'package:internet_connection_checker/internet_connection_checker.dart';

abstract class NetworkInfo {
  Future<bool> get isConnected;
}

class NetworkInfoImp implements NetworkInfo {
  final InternetConnectionChecker _connectionChecker;

  NetworkInfoImp(this._connectionChecker);
  @override
  // TODO: implement isConnected
  Future<bool> get isConnected => _connectionChecker.hasConnection;
}
