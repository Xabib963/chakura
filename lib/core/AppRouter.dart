import 'package:chakura/features/Order/presentation/Blocs/bloc/orders_bloc_bloc.dart';
import 'package:chakura/features/Order/presentation/page/ConfirmOrder.dart';
import 'package:chakura/features/Order/presentation/page/Orders.dart';
import 'package:chakura/features/Products/presentation/Blocs/bloc/addto_hive_bloc.dart';
import 'package:chakura/features/Products/presentation/Cubit/chooseTypeCubit.dart/cubit/choose_type_cubit.dart';
import 'package:chakura/features/Products/presentation/Page/EquipmentDiatels.dart';
import 'package:chakura/features/Products/presentation/Page/Equipments.dart';
import 'package:chakura/features/Products/presentation/Page/MyProducts.dart';
import 'package:chakura/features/Profile/presintation/Screen/Profile.dart';
import 'package:chakura/features/Profile/presintation/bloc/user_profile_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../InjectionContainer.dart';
import '../SplashScreen.dart';

import '../features/GetIn/presentaion/bloc/splass_bloc.dart';
import '../features/GetIn/presentaion/cubit/update_avatar_cubit.dart';
import '../features/GetIn/presentaion/views/AddUserInfo.dart';
import '../features/Products/presentation/Blocs/bloc/get_add_up_delete_bloc.dart';
import '../features/Products/presentation/Cubit/Navigationcubit/navidation_cubit.dart';
import '../features/Products/presentation/Page/AddNewProduct.dart';
import '../features/Products/presentation/Widget/CustombottomBar.dart';
import '../features/GetIn/presentaion/views/Login.dart';

class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => BlocProvider<SplassBloc>(
                create: (context) => SplassBloc()..add(const SplashEvent()),
                child: BlocListener<SplassBloc, SplassState>(
                    listener: (context, state) {
                      if (state is NavigateState) {
                        if (dependecy<SharedPreferences>()
                                .getString("userId") !=
                            null) {
                          Navigator.of(context).pushReplacementNamed(
                            CustomBottomBar.routeName,
                          );
                        } else {
                          Navigator.of(context).pushReplacementNamed(
                            LoginScreen.routeName,
                          );
                        }
                      }
                      ;
                    },
                    child: const SplashScreen())));
      case '/logins':
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => LoginScreen(),
        );
      case '/AddUserinfo':
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => BlocProvider<UpdateAvatarCubit>(
            create: (context) => UpdateAvatarCubit(),
            child: AdduserInfo(),
          ),
        );
      case '/myProducts':
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => MyProducts(),
        );
      case '/equiomentDeteils':
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider.value(value: context.read<GetAddUpDeleteBloc>())
            ],
            child: EquipmentDiatels(),
          ),
        );
      case '/addProduct':
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider.value(value: context.read<GetAddUpDeleteBloc>()),
              BlocProvider(create: (_) => dependecy<ChooseTypeCubit>())
            ],
            child: AddProductScreen(
              title: "Add Product",
              value: null,
            ),
          ),
        );
      case '/landing':
        return MaterialPageRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider<NavidationCubit>(
                create: (context) => NavidationCubit(),
              ),
              BlocProvider<AddtoHiveBloc>(
                create: (context) => dependecy<AddtoHiveBloc>(),
              ),
            ],
            child: CustomBottomBar(),
          ),
        );
      case '/Order':
        return MaterialPageRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider<OrdersBloc>(
                create: (context) => dependecy<OrdersBloc>(),
              ),
            ],
            child: Order(),
          ),
        );
      case '/ConfirmOrder':
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => BlocProvider<OrdersBloc>(
            create: (context) => dependecy<OrdersBloc>(),
            child: ConfirmOrder(),
          ),
        );
      case '/profileScreen':
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => BlocProvider<UserProfileBloc>(
            create: (context) => dependecy<UserProfileBloc>(),
            child: ProfileScreen(),
          ),
        );
      default:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => Equipments(),
        );
    }
  }
}
