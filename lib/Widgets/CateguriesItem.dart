import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../core/themes/Thems.dart';
import '../features/GetIn/presentaion/cubit/categuries_cubit_cubit.dart';

CateguriesCubitCubit cat = CateguriesCubitCubit();

class CateiguriesItem extends StatefulWidget {
  CateiguriesItem({super.key});

  @override
  State<CateiguriesItem> createState() => _CateiguriesItemState();
}

class _CateiguriesItemState extends State<CateiguriesItem> {
  List<IconData> icon = [
    FontAwesomeIcons.computer,
    FontAwesomeIcons.chair,
    Icons.speed_rounded
  ];

  List active = ['Computer'];

  List categuries = [
    'Computer',
    'Chairs',
    'Buzzeles',
  ];

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Discover :',
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontFamily: 'Poppins',
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
            ),
          ),
          SizedBox(
              height: media.height * 0.14,
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: categuries.length,
                itemBuilder: (context, index) => Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(15),
                    onTap: () {
                      active.clear();
                      active.add(categuries[index]);

                      index == 0 ? cat.categ1() : cat.categ2();
                      setState(() {});
                    },
                    child: Container(
                      width: active.contains(categuries[index])
                          ? media.width * 0.21
                          : media.width * 0.20,
                      height: active.contains(categuries[index])
                          ? media.height * 0.13
                          : media.height * 0.11,
                      decoration: BoxDecoration(
                        // color: active.contains(categuries[index])
                        //     ? ColorTheme.main
                        //     : Colors.grey.withOpacity(0.5),
                        borderRadius: BorderRadius.circular(20),
                        shape: BoxShape.rectangle,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: media.width * 0.18,
                            height: media.height * 0.1,
                            child: Card(
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              color: ColorTheme.secondary,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50),
                              ),
                              child: Center(
                                child: Icon(
                                  icon[index],
                                  color: Colors.white,
                                  size: 25,
                                ),
                              ),
                            ),
                          ),
                          Text(
                            categuries[index],
                            style:
                                Theme.of(context).textTheme.bodyText1!.copyWith(
                                      fontFamily: 'Poppins',
                                      color: active.contains(categuries[index])
                                          ? Color.fromARGB(255, 253, 253, 253)
                                          : Colors.white,
                                      fontSize: 12,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )),
        ],
      ),
    );
  }
}
