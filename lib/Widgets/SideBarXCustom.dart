import 'package:chakura/features/GetIn/presentaion/views/Login.dart';
import 'package:chakura/features/Products/presentation/Page/MyProducts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:sidebarx/sidebarx.dart';

import '../InjectionContainer.dart';
import '../core/themes/Thems.dart';
import '../features/Order/presentation/page/Orders.dart';
import '../features/Products/presentation/Page/AddNewProduct.dart';
import '../features/Profile/presintation/Screen/Profile.dart';

class SideBarXWidget extends StatelessWidget {
  const SideBarXWidget({
    super.key,
    required SidebarXController controller,
  }) : _controller = controller;

  final SidebarXController _controller;

  @override
  Widget build(BuildContext context) {
    return SidebarX(
      headerDivider: Divider(
        endIndent: 20.w,
        indent: 20.w,
        color: Colors.grey,
      ),
      footerBuilder: (context, extended) => IconButton(
          onPressed: () {
            dependecy<SharedPreferences>().clear();
            Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
          },
          icon: const Icon(
            Icons.login_sharp,
            color: Colors.white,
          )),
      controller: _controller,
      theme: SidebarXTheme(
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: ColorTheme.main.withOpacity(0.8),
          borderRadius: BorderRadius.circular(20),
        ),
        hoverColor: Colors.white12,
        textStyle: TextStyle(color: Colors.white.withOpacity(0.7)),
        selectedTextStyle: const TextStyle(color: Colors.white),
        itemTextPadding: const EdgeInsets.only(left: 30),
        selectedItemTextPadding: const EdgeInsets.only(left: 30),
        itemDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.white.withOpacity(0.7)),
        ),
        selectedItemDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: ColorTheme.mainLight.withOpacity(0.37),
          ),
          gradient: LinearGradient(
            colors: [
              ColorTheme.mainLight,
              ColorTheme.mainLight.withOpacity(0.5)
            ],
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.white.withOpacity(0.28),
              blurRadius: 5,
            )
          ],
        ),
        iconTheme: IconThemeData(
          color: Colors.white.withOpacity(0.7),
          size: 20,
        ),
        selectedIconTheme: const IconThemeData(
          color: Colors.white,
          size: 20,
        ),
      ),
      extendedTheme: SidebarXTheme(
        width: 200,
        decoration: BoxDecoration(
          color: ColorTheme.main.withOpacity(0.9),
        ),
      ),
      footerDivider: Divider(
        color: Colors.white,
      ),
      headerBuilder: (context, extended) {
        return SizedBox(
          height: 200,
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Image.asset(
                'assets/${dependecy<SharedPreferences>().getString('img') ?? 'batman.png'}'),
          ),
        );
      },
      items: [
        SidebarXItem(
          icon: Icons.category_rounded,
          label: 'My Prouducts',
          onTap: () => Navigator.of(context).pushNamed(MyProducts.routeName),
        ),
        SidebarXItem(
          icon: Icons.add_circle_outline_rounded,
          label: 'Add Products',
          onTap: () =>
              Navigator.of(context).pushNamed(AddProductScreen.routeName),
        ),
        SidebarXItem(
          icon: Icons.offline_pin_outlined,
          label: 'My Orders',
          onTap: () => Navigator.of(context).pushNamed(Order.routeName),
        ),
        SidebarXItem(
          icon: Icons.people,
          onTap: () {
            Navigator.of(context).pushNamed(ProfileScreen.routeName);
          },
          label: 'My Profile',
        ),
      ],
    );
  }
}
