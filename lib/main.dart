import 'package:chakura/core/AppRouter.dart';
import 'package:chakura/features/GetIn/presentaion/bloc/loginBloc/bloc/get_in_bloc.dart';
import 'package:chakura/features/Products/data/DataProviders/LocalDB.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'features/GetIn/presentaion/cubit/add_tofavorites_cubit.dart';

import 'InjectionContainer.dart';
import 'SplashScreen.dart';

import 'InjectionContainer.dart' as dependency;
import 'core/themes/Thems.dart';
import 'features/Products/presentation/Blocs/bloc/addto_hive_bloc.dart';

import 'features/Products/presentation/Blocs/bloc/get_add_up_delete_bloc.dart';
import 'features/GetIn/presentaion/views/Login.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await dependency.init();
  // dependency.dependecy<LocalDB>();

  runApp(MyApp());
}

AddTofavoritesCubit favoriteCubit = AddTofavoritesCubit();

class MyApp extends StatelessWidget {
  MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => dependecy<AddtoHiveBloc>(),
        ),
        BlocProvider(
          create: (context) => dependecy<GetInBloc>(),
        ),
        BlocProvider<GetAddUpDeleteBloc>(
          create: (context) => dependecy<GetAddUpDeleteBloc>(),
        ),
      ],
      child: ScreenUtilInit(
          minTextAdapt: true,
          splitScreenMode: true,
          useInheritedMediaQuery: true,
          // Use builder only if you need to use library outside ScreenUtilInit context
          builder: (_, __) {
            return MaterialApp(
                debugShowCheckedModeBanner: false,
                title: 'Chakura Store',
                onGenerateRoute: AppRouter.onGenerateRoute,
                theme: ThemeData(
                    splashColor: Colors.transparent,
                    useMaterial3: true,
                    scaffoldBackgroundColor: ColorTheme.main,
                    primarySwatch: Colors.grey,
                    canvasColor: ColorTheme.main),
                initialRoute: SplashScreen.routeName);
          }),
    );
  }
}
