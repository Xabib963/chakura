// import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import '../../Widgets/CateguriesItem.dart';
// import '../../main.dart';
// import '../../core/themes/Thems.dart';

// class EquipmentDiatels extends StatefulWidget {
//   EquipmentDiatels({super.key});

//   @override
//   State<EquipmentDiatels> createState() => _EquipmentDiatelsState();
// }

// class _EquipmentDiatelsState extends State<EquipmentDiatels> {
//   @override
//   Widget build(BuildContext context) {
//     Size media = MediaQuery.of(context).size;
//     return Scaffold(
//       body: SafeArea(
//           child: Column(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: [
//           SizedBox(
//             width: media.width,
//             height: media.height * 0.5,
//             child: Container(
//               decoration: BoxDecoration(
//                   borderRadius:
//                       BorderRadius.vertical(bottom: Radius.circular(25)),
//                   gradient: LinearGradient(
//                       begin: Alignment.bottomCenter,
//                       end: Alignment.topLeft,
//                       colors: [
//                         ColorTheme.mainOpa..withOpacity(0.5),
//                         ColorTheme.mainLight..withOpacity(0.8),
//                         ColorTheme.main..withOpacity(0.5),
//                       ])),
//               child: Card(
//                   color: Colors.transparent,
//                   margin: EdgeInsets.zero,
//                   shape: const RoundedRectangleBorder(
//                       borderRadius:
//                           BorderRadius.vertical(bottom: Radius.circular(25))),
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     children: [
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           IconButton(
//                               onPressed: () {
//                                 Navigator.of(context).pop();
//                               },
//                               icon: const Icon(
//                                 Icons.arrow_back_ios_new,
//                                 color: Colors.white,
//                               )),
//                           SizedBox(
//                             width: media.width * 0.40,
//                             child: const Text(
//                               'Dieatels',
//                               overflow: TextOverflow.ellipsis,
//                               style: TextStyle(
//                                 color: Colors.white,
//                                 fontWeight: FontWeight.w400,
//                                 fontSize: 20,
//                               ),
//                             ),
//                           ),
//                           SizedBox(
//                             width: media.width * 0.02,
//                           )
//                         ],
//                       ),
//                       Expanded(
//                         child: Image.asset(
//                           cat.image,
//                           width: media.width * 0.4,
//                         ),
//                       )
//                     ],
//                   )),
//             ),
//           ),
//           SizedBox(
//             width: media.width * 0.95,
//             height: media.height * 0.45,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Column(
//                   mainAxisAlignment: MainAxisAlignment.spaceAround,
//                   children: [
//                     Row(
//                       children: [
//                         SizedBox(
//                           width: media.width * 0.7,
//                           child: const Text(
//                             'Tankum minato chaires',
//                             overflow: TextOverflow.ellipsis,
//                             style: TextStyle(
//                               color: Colors.white,
//                               fontWeight: FontWeight.w400,
//                               fontSize: 25,
//                             ),
//                           ),
//                         ),
//                         SizedBox(
//                           width: media.width * 0.25,
//                           child: Text(
//                             '100\$',
//                             overflow: TextOverflow.ellipsis,
//                             style: TextStyle(
//                               color: Colors.green[800],
//                               fontWeight: FontWeight.w600,
//                               fontSize: 20,
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                     Row(
//                       children: [
//                         const Text(
//                           '14478 reviews',
//                           overflow: TextOverflow.ellipsis,
//                           style: TextStyle(
//                             color: Color.fromARGB(255, 133, 133, 133),
//                             fontWeight: FontWeight.w400,
//                             fontSize: 15,
//                           ),
//                         ),
//                         SizedBox(width: media.width * 0.05),
//                         SizedBox(
//                           width: media.width * 0.25,
//                           child: const Row(
//                             children: [
//                               Icon(
//                                 Icons.star,
//                                 color: Colors.amber,
//                               ),
//                               Text(
//                                 '4.7',
//                                 overflow: TextOverflow.ellipsis,
//                                 style: TextStyle(
//                                   color: Colors.white,
//                                   fontWeight: FontWeight.w300,
//                                   fontSize: 18,
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ],
//                     ),
//                   ],
//                 ),
//                 Padding(
//                   padding:
//                       const EdgeInsets.symmetric(vertical: 5, horizontal: 2),
//                   child: SizedBox(
//                     width: media.width * 0.98,
//                     height: media.height * 0.25,
//                     child: SingleChildScrollView(
//                       child: RichText(
//                           text: const TextSpan(
//                               style: TextStyle(
//                                 color: Colors.white70,
//                                 fontSize: 13,
//                               ),
//                               children: [
//                             TextSpan(
//                                 text:
//                                     'Adipisicing quis ea dolore est minim labore sit non enim adipisicing nisi. Cillum ull'),
//                             TextSpan(
//                                 text:
//                                     'Adipisicing quis ea dolore est minim labore sit non enim adipisicing nisi. Cillum ull'),
//                             TextSpan(
//                                 text:
//                                     'Adipisicing quis ea dolore est minim labore sit non enim adipisicing nisi. Cillum ull'),
//                             TextSpan(
//                                 text:
//                                     'Adipisicing quis ea dolore est minim labore sit non enim adipisicing nisi. Cillum ull'),
//                             TextSpan(
//                                 text:
//                                     'Adipisicing quis ea dolore est minim labore sit non enim adipisicing nisi. Cillum ull'),
//                             TextSpan(
//                                 text:
//                                     'amco enim minim ipsum veniam consequat. Commodo eu sit adipisicing mollit ut.Eiusmod tempor id et enim esse quis consequat fugiat amet voluptate aute do aliquip. Nisi qui sint nisi occaecat exercitation fugiat voluptate cillum exercitation ullamco ea qui cillum incididunt. Irure excepteur dolor et id duis adipisicing duis. Esse voluptate duis qui ullamco sunt id laborum commodo Lorem laboris occaecat pariatur est eiusmod.')
//                           ])),
//                     ),
//                   ),
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceAround,
//                   children: [
//                     InkWell(
//                       onTap: () {
//                         favoriteCubit.addOrRemovefav(cat.image);
//                         setState(() {});
//                       },
//                       child: Container(
//                           width: media.width * 0.15,
//                           height: media.width * 0.15,
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(50),
//                             color: ColorTheme.main,
//                             // border: Border.all(
//                             //     width: 1, color: ColorTheme.additional),
//                           ),
//                           child: Center(
//                               child: Icon(
//                                   size: 35,
//                                   color: Colors.white,
//                                   favoriteCubit.fav.contains(cat.image)
//                                       ? Icons.favorite
//                                       : Icons.favorite_border))),
//                     ),
//                     Container(
//                         width: media.width * 0.65,
//                         height: media.width * 0.15,
//                         decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(50),
//                             gradient: LinearGradient(
//                                 begin: Alignment.bottomCenter,
//                                 end: Alignment.topLeft,
//                                 colors: [
//                                   ColorTheme.mainLight..withOpacity(0.5),
//                                   ColorTheme.mainLight..withOpacity(0.5),
//                                   ColorTheme.additional..withOpacity(0.5),
//                                 ])),
//                         child: const Center(
//                             child: Text(
//                           'Add to Cart',
//                           style: TextStyle(
//                             color: Colors.white,
//                             fontSize: 20,
//                           ),
//                         ))),
//                   ],
//                 )
//               ],
//             ),
//           ),
//         ],
//       )),
//     );
//   }
// }
