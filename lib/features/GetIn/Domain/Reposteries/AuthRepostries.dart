import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';

abstract class AuthRepostries {
  Future<Either<Failure, Unit>> logIn(String email, String password);
  Future<Either<Failure, Unit>> register(
      String email,
      String password,
      String name,
      String age,
      String address,
      String job,
      String phone,
      String img);
}
