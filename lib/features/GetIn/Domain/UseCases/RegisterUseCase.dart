import 'package:chakura/features/GetIn/Data/DataProvider/Auth.dart';
import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../Products/Domain/entities/Product.dart';
import '../Reposteries/AuthRepostries.dart';

class RegisterUseCase {
  final AuthRepostries _authRepostries;

  RegisterUseCase(this._authRepostries);
  Future<Either<Failure, Unit>> call(String email, String password, String name,
      String age, String address, String job, String phone, String img) {
    return _authRepostries.register(
        email, password, name, age, address, job, phone, img);
  }
}
