import 'package:chakura/features/GetIn/Data/DataProvider/Auth.dart';
import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../Products/Domain/entities/Product.dart';
import '../Reposteries/AuthRepostries.dart';

class LoginUseCase {
  final AuthRepostries _authRepostries;

  LoginUseCase(this._authRepostries);
  Future<Either<Failure, Unit>> call(String email, String password) {
    return _authRepostries.logIn(email, password);
  }
}
