part of 'splass_bloc.dart';

abstract class SplassState extends Equatable {
  const SplassState();

  @override
  List<Object> get props => [];
}

class SplassInitial extends SplassState {}

class NavigateState extends SplassState {}
