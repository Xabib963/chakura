import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'splass_event.dart';
part 'splass_state.dart';

class SplassBloc extends Bloc<SplassEvent, SplassState> {
  SplassBloc() : super(SplassInitial()) {
    on<SplashEvent>((event, emit) async {
      print('as');
      await Future.delayed(const Duration(milliseconds: 3000));
      emit(NavigateState());
    });
  }
}
