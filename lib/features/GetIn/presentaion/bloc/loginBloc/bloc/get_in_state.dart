part of 'get_in_bloc.dart';

sealed class GetInState extends Equatable {
  const GetInState();

  @override
  List<Object> get props => [];
}

final class GetInInitial extends GetInState {}

final class Authurized extends GetInState {}

final class NotAuthurized extends GetInState {
  final String messege;

  NotAuthurized({required this.messege});
}

final class RAuthurized extends GetInState {}

final class RNotAuthurized extends GetInState {
  final String messege;

  RNotAuthurized({required this.messege});
}

final class RegisterdState extends GetInState {}
