import 'package:bloc/bloc.dart';
import 'package:chakura/features/GetIn/Domain/UseCases/LoginUsecase.dart';
import 'package:equatable/equatable.dart';

import '../../../../../../core/errors/failures.dart';
import '../../../../Domain/UseCases/RegisterUseCase.dart';

part 'get_in_event.dart';
part 'get_in_state.dart';

class GetInBloc extends Bloc<GetInEvent, GetInState> {
  final LoginUseCase _loginUseCase;
  final RegisterUseCase registerUseCase;
  bool loading = false;

  GetInBloc(this._loginUseCase, this.registerUseCase) : super(GetInInitial()) {
    on<GetInEvent>((event, emit) async {
      loading = true;
      emit(GetInInitial());
      var authed = await _loginUseCase(event.e_mail!, event.password!);
      authed.fold((l) {
        loading = false;
        emit(NotAuthurized(messege: _mapFailureTomessege(l)));
      }, (r) {
        loading = false;
        emit(Authurized());
      });
    });
    on<RegisterEvent>((event, emit) async {
      emit(GetInInitial());
      var authed = await registerUseCase(
          event.e_mail!,
          event.password!,
          event.name,
          event.age,
          event.address,
          event.job,
          event.phone,
          event.img);
      authed.fold((l) => emit(RNotAuthurized(messege: _mapFailureTomessege(l))),
          (r) => emit(RAuthurized()));
    });
  }
  String _mapFailureTomessege(Failure fail) {
    switch (fail.runtimeType) {
      case ServicesFailure:
        return '';
      case DataSourceFailure:
        return '';
      case NOTAUTHEDFailure:
        return 'Email or password not Correct';
      case OffLineFailure:
        return 'please Check your connection';
      default:
        return "UnEcpected Error";
    }
  }
}
