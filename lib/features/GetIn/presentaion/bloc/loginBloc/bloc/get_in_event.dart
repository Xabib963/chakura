part of 'get_in_bloc.dart';

class GetInEvent extends Equatable {
  const GetInEvent(this.e_mail, this.password);
  final String? e_mail;
  final String? password;
  @override
  List<Object> get props => [];
}

class RegisterEvent extends GetInEvent {
  RegisterEvent(super.e_mail, super.password, this.name, this.age, this.job,
      this.address, this.phone, this.img);
  final img;
  final name;
  final age;
  final job;
  final address;
  final phone;

  @override
  List<Object> get props => [phone, address, job, age, name];
}
