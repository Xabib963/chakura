part of 'splass_bloc.dart';

abstract class SplassEvent extends Equatable {
  const SplassEvent();

  @override
  List<Object> get props => [];
}

class SplashEvent extends SplassEvent {
  const SplashEvent();

  @override
  List<Object> get props => [];
}
