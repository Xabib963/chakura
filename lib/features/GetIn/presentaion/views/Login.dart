import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../InjectionContainer.dart';
import '../../../../core/themes/Thems.dart';
import '../../../Products/presentation/Widget/CustombottomBar.dart';
import '../bloc/loginBloc/bloc/get_in_bloc.dart';
import '../widgets/CustomTextForm.dart';
import 'AddUserInfo.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({super.key});
  static const routeName = '/logins';

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  TextEditingController _emailcontroller = TextEditingController();

  TextEditingController _passwordcontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldState,
      resizeToAvoidBottomInset: true,
      backgroundColor: ColorTheme.main,
      body: Padding(
        padding: EdgeInsets.all(5),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                width: double.infinity,
                height: media.height * 0.55,
                child: Stack(
                  children: [
                    Lottie.asset('animation/125329-gaming-vr-blue.json',
                        width: media.width, height: media.height * 0.6),
                    Positioned(
                      top: media.height * 0.45,
                      right: media.width * 0.18,
                      child:
                          AnimatedTextKit(repeatForever: true, animatedTexts: [
                        TypewriterAnimatedText(
                          'Login',
                          textStyle: TextStyle(
                              fontSize: 45.0.sp,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey[350]),
                          speed: const Duration(milliseconds: 500),
                        ),
                      ]),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 330.w,
                child: Padding(
                  padding: EdgeInsets.all(8.0.w),
                  child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      physics: NeverScrollableScrollPhysics(),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 20.h,
                          ),
                          CustomTextForm(
                            sympol: '@',
                            controller: _emailcontroller,
                            ispassowrd: false,
                            label: "E-Mail",
                            prefix: Icons.email,
                            suffix: null,
                            onpressed: () {
                              ;
                            },
                          ),
                          SizedBox(
                            height: 20.h,
                          ),
                          StatefulBuilder(builder: (context, setStates) {
                            return CustomTextForm(
                              sympol: '',
                              controller: _passwordcontroller,
                              ispassowrd: true,
                              label: "password",
                              prefix: Icons.lock,
                              suffix: null,
                              onpressed: () {
                                setStates(
                                  () {},
                                );
                              },
                            );
                          }),
                          BlocListener<GetInBloc, GetInState>(
                            listener: (context, state) {
                              if (state is Authurized) {
                                if (dependecy<SharedPreferences>()
                                        .getString("token") !=
                                    null) {
                                  setState(() {});
                                  (() {},);
                                  Navigator.of(context).pushReplacementNamed(
                                      CustomBottomBar.routeName);
                                } else {
                                  setState(
                                    () {},
                                  );
                                  final snackBar = SnackBar(
                                    elevation: 0,
                                    behavior: SnackBarBehavior.floating,
                                    backgroundColor: Colors.transparent,
                                    content: AwesomeSnackbarContent(
                                      title: 'Congrats!',
                                      message: "not registerd",

                                      /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
                                      contentType: ContentType.success,
                                    ),
                                  );

                                  ScaffoldMessenger.of(context)
                                    ..hideCurrentSnackBar()
                                    ..showSnackBar(snackBar);
                                }
                              }
                              if (state is NotAuthurized) {
                                setState(() {});
                                (() {},);
                                final snackBar = SnackBar(
                                  elevation: 0,
                                  behavior: SnackBarBehavior.floating,
                                  backgroundColor: Colors.transparent,
                                  content: AwesomeSnackbarContent(
                                    title: 'oh Snap!',
                                    message: state.messege,

                                    /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
                                    contentType: ContentType.failure,
                                  ),
                                );

                                ScaffoldMessenger.of(context)
                                  ..hideCurrentSnackBar()
                                  ..showSnackBar(snackBar);
                              }
                              // if (state is GetInInitial) {
                            },
                            child: Padding(
                              padding: EdgeInsets.all(20.0.w),
                              child: !context.read<GetInBloc>().loading
                                  ? ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          backgroundColor: ColorTheme.mainLight,
                                          fixedSize: Size(media.width * 0.35,
                                              media.height * 0.07)),
                                      onPressed: () async {
                                        if (_formKey.currentState!.validate()) {
                                          setState(() {});
                                          (() {},);
                                          context.read<GetInBloc>().add(
                                              GetInEvent(
                                                  _emailcontroller.text.trim(),
                                                  _passwordcontroller.text
                                                      .trim()));
                                        }
                                      },
                                      child: Text(
                                        'LOG IN',
                                        style: TextStyle(
                                            fontSize: 17.sp,
                                            fontWeight: FontWeight.bold,
                                            color: ColorTheme.additional2),
                                      ))
                                  : Center(
                                      child: CircularProgressIndicator(
                                      color: ColorTheme.mainLight,
                                    )),
                            ),
                          ),
                          TextButton(
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  Navigator.of(context).pushNamed(
                                      AdduserInfo.routeName,
                                      arguments: {
                                        'e_mail': _emailcontroller.text.trim(),
                                        'password':
                                            _passwordcontroller.text.trim()
                                      });
                                }
                              },
                              child: Text(
                                'New ? Register for free',
                                style: TextStyle(
                                    fontSize: 15.0.sp,
                                    fontWeight: FontWeight.bold,
                                    color: ColorTheme.mainOpa),
                              ))
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
