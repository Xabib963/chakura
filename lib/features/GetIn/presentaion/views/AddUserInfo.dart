import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:chakura/features/GetIn/presentaion/bloc/loginBloc/bloc/get_in_bloc.dart';
import 'package:chakura/features/GetIn/presentaion/cubit/update_avatar_cubit.dart';
import 'package:chakura/features/GetIn/presentaion/widgets/CustomTextForm.dart';
import 'package:chakura/features/Products/presentation/Cubit/ChooseImage/choose_image_cubit.dart';
import 'package:chakura/features/Products/presentation/Widget/CustombottomBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../InjectionContainer.dart';
import '../../../../core/themes/Thems.dart';

class AdduserInfo extends StatelessWidget {
  AdduserInfo({super.key});
  static const routeName = '/AddUserinfo';

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController ageController = TextEditingController();
  TextEditingController jobController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.of(context).size;
    Map<String, dynamic> args =
        ModalRoute.of(context)!.settings.arguments! as Map<String, dynamic>;
    return Scaffold(
      backgroundColor: ColorTheme.main,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            )),
        centerTitle: true,
        title: Text(
          'I N F O',
          style: textStyle(
              size: 20.sp, color: Colors.white, weight: FontWeight.bold),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Text(
                'Choose your Avatar',
                style: textStyle(
                    size: 25.sp,
                    color: Colors.white,
                    weight: FontWeight.normal),
              ),
            ),
            BlocBuilder<UpdateAvatarCubit, UpdateAvatarState>(
                builder: (context, state) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  InkWell(
                    onTap: () {
                      context
                          .read<UpdateAvatarCubit>()
                          .updateIndex(1, 'batman.png');
                    },
                    child: CircleAvatar(
                      backgroundColor:
                          context.read<UpdateAvatarCubit>().indexImg == 1
                              ? Colors.greenAccent
                              : Colors.white,
                      backgroundImage: AssetImage('assets/batman.png'),
                      radius: 40.r,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      context
                          .read<UpdateAvatarCubit>()
                          .updateIndex(2, 'old_man.png');
                    },
                    child: CircleAvatar(
                      backgroundColor:
                          context.read<UpdateAvatarCubit>().indexImg == 2
                              ? Colors.greenAccent
                              : Colors.white,
                      backgroundImage: AssetImage('assets/old_man.png'),
                      radius: 40.r,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      context
                          .read<UpdateAvatarCubit>()
                          .updateIndex(3, 'robo.png');
                    },
                    child: CircleAvatar(
                      backgroundColor:
                          context.read<UpdateAvatarCubit>().indexImg == 3
                              ? Colors.greenAccent
                              : Colors.white,
                      backgroundImage: AssetImage('assets/robo.png'),
                      radius: 40.r,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      context
                          .read<UpdateAvatarCubit>()
                          .updateIndex(4, 'gym.png');
                    },
                    child: CircleAvatar(
                      backgroundColor:
                          context.read<UpdateAvatarCubit>().indexImg == 4
                              ? Colors.greenAccent
                              : Colors.white,
                      backgroundImage: AssetImage('assets/gym.png'),
                      radius: 40.r,
                    ),
                  )
                ],
              );
            }),
            Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 20.0.w, vertical: 20.w),
                        child: CustomTextForm(
                          sympol: '',
                          onpressed: () {},
                          prefix: null,
                          suffix: null,
                          controller: nameController,
                          label: 'Full Name',
                          ispassowrd: false,
                        )),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 20.0.w, vertical: 20.w),
                        child: CustomTextForm(
                          sympol: '',
                          onpressed: () {},
                          prefix: null,
                          suffix: null,
                          controller: addressController,
                          label: 'Address',
                          ispassowrd: false,
                        )),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 20.0.w, vertical: 20.w),
                        child: CustomTextForm(
                          sympol: '',
                          onpressed: () {},
                          prefix: null,
                          suffix: null,
                          number: true,
                          controller: ageController,
                          label: 'Age',
                          ispassowrd: false,
                        )),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 20.0.w, vertical: 20.w),
                        child: CustomTextForm(
                          sympol: '',
                          onpressed: () {},
                          prefix: null,
                          suffix: null,
                          controller: jobController,
                          label: 'Job Title',
                          ispassowrd: false,
                        )),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 20.0.w, vertical: 20.w),
                        child: CustomTextForm(
                          sympol: '',
                          onpressed: () {},
                          prefix: null,
                          suffix: null,
                          number: true,
                          controller: phoneController,
                          label: 'Phone Number',
                          ispassowrd: false,
                        )),
                  ],
                ),
              ),
            ),
            StatefulBuilder(builder: (context, setstate) {
              return BlocListener<GetInBloc, GetInState>(
                listener: (context, state) {
                  if (state is RAuthurized) {
                    setstate(
                      () {},
                    );
                    final snackBar = SnackBar(
                      elevation: 0,
                      behavior: SnackBarBehavior.floating,
                      backgroundColor: Colors.transparent,
                      content: AwesomeSnackbarContent(
                        title: 'Congrats!',
                        message: " registerd Successfully",

                        /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
                        contentType: ContentType.success,
                      ),
                    );

                    ScaffoldMessenger.of(context)
                      ..hideCurrentSnackBar()
                      ..showSnackBar(snackBar);
                    Navigator.of(context).pushNamed(CustomBottomBar.routeName);
                  }
                  if (state is RNotAuthurized) {
                    setstate(
                      () {},
                    );
                    final snackBar = SnackBar(
                      elevation: 0,
                      behavior: SnackBarBehavior.floating,
                      backgroundColor: Colors.transparent,
                      content: AwesomeSnackbarContent(
                        title: 'oh Snap!',
                        message: state.messege,

                        /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
                        contentType: ContentType.failure,
                      ),
                    );

                    ScaffoldMessenger.of(context)
                      ..hideCurrentSnackBar()
                      ..showSnackBar(snackBar);
                  }
                },
                child: !context.read<GetInBloc>().loading
                    ? ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            elevation: 10,
                            fixedSize: Size(160.w, 60.w),
                            disabledBackgroundColor: Colors.transparent,
                            backgroundColor: ColorTheme.secondary),
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            try {
                              setstate(
                                () {},
                              );
                              context.read<GetInBloc>().add(RegisterEvent(
                                    args['e_mail'],
                                    args['password'],
                                    nameController.text,
                                    ageController.text,
                                    jobController.text.trim(),
                                    addressController.text,
                                    phoneController.text,
                                    context.read<UpdateAvatarCubit>().iMg,
                                  ));
                            } catch (e) {
                              print(e);
                            }
                          }
                        },
                        child: Text(
                          overflow: TextOverflow.clip,
                          'Sign Up',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Poppins',
                              fontSize: 20.sp,
                              fontWeight: FontWeight.bold),
                        ))
                    : Center(
                        child: CircularProgressIndicator(
                        color: ColorTheme.secondary,
                      )),
              );
            })
          ]),
        ),
      ),
    );
  }
}
