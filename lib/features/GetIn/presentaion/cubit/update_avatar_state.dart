part of 'update_avatar_cubit.dart';

sealed class UpdateAvatarState extends Equatable {
  const UpdateAvatarState();

  @override
  List<Object> get props => [];
}

final class UpdateAvatarInitial extends UpdateAvatarState {}

final class UpdatedState extends UpdateAvatarState {}
