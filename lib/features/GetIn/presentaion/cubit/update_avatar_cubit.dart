import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'update_avatar_state.dart';

class UpdateAvatarCubit extends Cubit<UpdateAvatarState> {
  int indexImg = 0;
  String iMg = '';
  UpdateAvatarCubit() : super(UpdateAvatarInitial());
  updateIndex(index, img) {
    emit(UpdateAvatarInitial());
    indexImg = index;
    iMg = img;
    emit(UpdatedState());
  }
}
