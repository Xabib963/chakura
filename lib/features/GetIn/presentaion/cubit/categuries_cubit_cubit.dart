import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

List<Map<String, String>> chaiersAndtables = [
  {"cate": "Chaires", "image": 'assets/PngItem_53573.png'},
  {"cate": "Chaires", "image": 'assets/PngItem_4960613.png'},
  {"cate": "Chaires", "image": 'assets/PngItem_3510563.png'},
  {"cate": "Chaires", "image": 'assets/PngItem_3837828.png'},
  {"cate": "Chaires", "image": 'assets/PngItem_4183106.png'},
  {"cate": "Chaires", "image": 'assets/PngItem_4475794.png'},
];
List<Map> computerStuf = [
  {"cate": "Computer", "image": 'assets/PngItem_128416.png'},
  {"cate": "Computer", "image": 'assets/PngItem_509374.png'},
  {"cate": "Computer", "image": 'assets/PngItem_718787.png'},
  {"cate": "Computer", "image": 'assets/PngItem_719053.png'},
  {"cate": "Computer", "image": 'assets/PngItem_719936.png'},
  {"cate": "Computer", "image": 'assets/PngItem_720627.png'},
  {"cate": "Computer", "image": 'assets/PngItem_1757602.png'},
  {"cate": "Computer", "image": 'assets/PngItem_4182752.png'},
  {"cate": "Computer", "image": 'assets/PngItem_5752534.png'},
];

class CateguriesCubitCubit extends Cubit<List<Map>> {
  String image = '';

  CateguriesCubitCubit() : super(computerStuf);
  void categ1() {
    emit(computerStuf);
  }

  void categ2() {
    emit(chaiersAndtables);
  }
}
