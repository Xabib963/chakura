import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

class AddTofavoritesCubit extends Cubit<List<String>> {
  List<String> fav = [];
  AddTofavoritesCubit() : super([]);
  void addOrRemovefav(var item) {
    if (fav.contains(item)) {
      fav.remove(item);
      emit(fav);
    } else {
      fav.add(item);
      emit(fav);
    }
  }
}
