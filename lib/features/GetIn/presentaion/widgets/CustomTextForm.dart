import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../core/themes/Thems.dart';

class CustomTextForm extends StatefulWidget {
  CustomTextForm({
    super.key,
    required this.controller,
    required this.ispassowrd,
    this.suffix,
    this.prefix,
    required this.onpressed,
    required this.label,
    required this.sympol,
    this.number,
  });
  final TextEditingController controller;
  bool ispassowrd;
  final IconData? suffix;
  final IconData? prefix;
  final Function() onpressed;
  final String label;
  final String sympol;
  final bool? number;

  @override
  State<CustomTextForm> createState() => _CustomTextFormState();
}

class _CustomTextFormState extends State<CustomTextForm> {
  Widget? myWidget;
  bool whatWidget = false;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      // validator: validate,
      controller: widget.controller,
      obscureText: whatWidget,
      keyboardType: widget.number != null ? TextInputType.number : null,
      inputFormatters: widget.number != null
          ? <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
            ]
          : <TextInputFormatter>[],
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please enter some text';
        }
        if (!widget.ispassowrd) {
          if (value.length > 3 && !value.contains(widget.sympol)) {
            return 'Not Valid Email Format';
          }
        }
        return null;
      },

      decoration: InputDecoration(
        hintText: widget.label,
        prefixIcon: widget.prefix != null
            ? IconButton(
                icon: Icon(widget.prefix),
                onPressed: widget.onpressed,
              )
            : null,
        suffixIcon: widget.ispassowrd
            ? whatWidget
                ? IconButton(
                    icon: Icon(
                      Icons.remove_red_eye,
                      size: 15.sp,
                    ),
                    onPressed: () {
                      whatWidget = !whatWidget;

                      setState(() {});
                    })
                : IconButton(
                    icon: FaIcon(
                      FontAwesomeIcons.eyeSlash,
                      size: 20.sp,
                    ),
                    onPressed: () {
                      whatWidget = !whatWidget;

                      setState(() {});
                    })
            : null,
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.only(left: 12.w),
        hintStyle: TextStyle(color: Colors.black54, fontSize: 18.sp),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.r)),
            borderSide: BorderSide(width: 2, color: ColorTheme.mainLight)),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.r)),
            borderSide: BorderSide(width: 3, color: ColorTheme.mainLight)),
        disabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(width: 3),
          borderRadius: BorderRadius.all(Radius.circular(20.w)),
        ),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.w)),
            borderSide: BorderSide(width: 2, color: ColorTheme.mainLight)),
      ),
    );
  }
}
