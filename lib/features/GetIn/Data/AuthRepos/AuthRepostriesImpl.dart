import 'package:chakura/core/errors/failures.dart';
import 'package:chakura/features/GetIn/Data/DataProvider/Auth.dart';
import 'package:chakura/features/GetIn/Domain/Reposteries/AuthRepostries.dart';
import 'package:dartz/dartz.dart';

import '../../../../core/Network/connection_info.dart';
import '../../../../core/errors/Exception.dart';

class AuthRepositriesImple implements AuthRepostries {
  final NetworkInfo networkInfo;
  final AuthProvider _authProvider;

  AuthRepositriesImple(this.networkInfo, this._authProvider);
  @override
  Future<Either<Failure, Unit>> logIn(String email, String password) async {
    if (await networkInfo.isConnected) {
      try {
        await _authProvider.login(email: email, password: password);
        return const Right(unit);
      } on NOTAUTHEDEXCEPTION {
        return Left(NOTAUTHEDFailure());
      }
    } else {
      // if i had a local data i need to try get products if null throw error
      return Left(OffLineFailure());
    }
    throw UnimplementedError();
  }

  @override
  Future<Either<Failure, Unit>> register(
      String email,
      String password,
      String name,
      String age,
      String address,
      String job,
      String phone,
      String img) async {
    if (await networkInfo.isConnected) {
      try {
        await _authProvider.register(
            email: email,
            password: password,
            address: address,
            age: age,
            job: job,
            name: name,
            phone: phone,
            img: img);
        return const Right(unit);
      } on NOTAUTHEDEXCEPTION {
        return Left(NOTAUTHEDFailure());
      }
    } else {
      // if i had a local data i need to try get products if null throw error
      return Left(OffLineFailure());
    }
  }
}
