import 'dart:async';
import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/errors/Exception.dart';

class AuthProvider {
  final SharedPreferences sharedPreferences;
  bool loading = false;
  String? _token;
  DateTime? _expiryDate;
  String? _userId;

  Timer? _authTimer;

  AuthProvider(this.sharedPreferences);

  bool get isAuth {
    return token != null;
  }

  String? get token {
    if (_expiryDate != null &&
        _expiryDate!.isAfter(DateTime.now()) &&
        _token != null) {
      return _token;
    }
    return null;
  }

  String? get userId {
    return _userId;
  }

  Future<Unit> login({required String email, required String password}) async {
    const url =
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDUdGZ6S0Zp4BWBcrJh6uqgSIdGJc-HmR0";

    try {
      final response = await http.post(Uri.parse(url),
          body: json.encode({
            'email': email,
            'password': password,
            'returnSecureToken': true
          }));
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        if (responseData['error']['message']
            .toString()
            .contains("INVALID_EMAIL")) {
          // Get.snackbar("EMAIL NOT FOUND", "Sorry");
        }
        if (responseData['error']['message']
            .toString()
            .contains("INVALID_PASSWORD")) {}
        if (responseData['error']['message']
            .toString()
            .contains("USER_DISABLED")) {}
      }

      _token = responseData['idToken'];

      _userId = responseData['localId'];
      print(_userId);
      sharedPreferences.setString('token', _token!);
      sharedPreferences.setString('userId', _userId!);

      _expiryDate = DateTime.now().add(Duration(
        seconds: int.parse(
          responseData['expiresIn'],
        ),
      ));
      return unit;
    } catch (e) {
      throw NOTAUTHEDEXCEPTION();
    }
  }

  Future<Unit> register(
      {required String email,
      required String password,
      required String name,
      required String age,
      required String address,
      required String job,
      required String phone,
      required String img}) async {
    const url =
        "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDUdGZ6S0Zp4BWBcrJh6uqgSIdGJc-HmR0";

    try {
      final response = await http.post(Uri.parse(url),
          body: json.encode({
            'email': email,
            'password': password,
            'returnSecureToken': true
          }));
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        if (responseData['error']['message']
            .toString()
            .contains("INVALID_EMAIL")) {
          return unit;
          // Get.snackbar("EMAIL NOT FOUND", "Sorry");
        }
        if (responseData['error']['message']
            .toString()
            .contains("INVALID_PASSWORD")) {
          return unit;
        }
        if (responseData['error']['message']
            .toString()
            .contains("USER_DISABLED")) {
          return unit;
        }
        return unit;
      }

      _token = responseData['idToken'];

      _userId = responseData['localId'];
      print(_userId);
      sharedPreferences.setString('token', _token!);
      sharedPreferences.setString('userId', _userId!);

      _expiryDate = DateTime.now().add(Duration(
        seconds: int.parse(
          responseData['expiresIn'],
        ),
      ));
      try {
        final response = await http.post(
            Uri.parse(
                'https://chakura-5bb7f-default-rtdb.asia-southeast1.firebasedatabase.app/Users.json'),
            body: json.encode({
              'name': name,
              'email': email,
              'age': age,
              'phone': phone,
              'job': job,
              'address': address,
              'password': password,
              'img': img,
              'userId': _userId
            }));
      } catch (e) {}
      return unit;
    } catch (e) {
      throw NOTAUTHEDEXCEPTION();
    }
  }
}
