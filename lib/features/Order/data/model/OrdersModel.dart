import '../../Domain/entities/Orders.dart';
import 'dart:convert';

class OrdersModel extends Orders {
  OrdersModel(
      {super.id,
      required super.fullName,
      required super.address,
      required super.phoneNumber,
      required super.products,
      required super.total,
      required super.status});

  factory OrdersModel.fromJson(Map<String, dynamic> json, String id) {
    return OrdersModel(
        id: id,
        fullName: json['fullName'],
        address: json['address'],
        phoneNumber: json['phoneNumber'],
        products: json['products'],
        total: json['total'],
        status: json['status']);
  }
}
