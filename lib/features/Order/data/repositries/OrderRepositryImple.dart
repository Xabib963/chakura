// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import 'package:chakura/core/errors/failures.dart';
import 'package:chakura/features/Order/Domain/entities/Orders.dart';

import '../../../../core/Network/connection_info.dart';
import '../../Domain/repositries/OrderREpositries.dart';
import '../Data Sources/OrderRemoteData.dart';

class OrderRepositryImple implements OrderRepositry {
  final NetworkInfo networkInfo;
  final OrderRemoteDataSourceImplemintation orderRemoteDataSourceImplemintation;
  OrderRepositryImple({
    required this.networkInfo,
    required this.orderRemoteDataSourceImplemintation,
  });
  @override
  Future<Either<Failure, Unit>> addOrder(Orders order) async {
    if (await networkInfo.isConnected) {
      try {
        await orderRemoteDataSourceImplemintation.add_Order(order);
        return Right(unit);
      } catch (e) {
        return Left(ServicesFailure());
      }
    } else {
      return Left(OffLineFailure());
    }
  }

  @override
  Future<Either<Failure, List<Orders>>> getOrders() async {
    if (await networkInfo.isConnected) {
      try {
        List<Orders> orders =
            await orderRemoteDataSourceImplemintation.getOrders();
        return Right(orders);
      } catch (e) {
        return Left(ServicesFailure());
      }
    } else {
      return Left(OffLineFailure());
    }
  }
}
