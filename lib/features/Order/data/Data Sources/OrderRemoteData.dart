import 'dart:convert';

import 'package:chakura/InjectionContainer.dart';
import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/Constants.dart';
import '../../../../core/errors/Exception.dart';
import '../../../Products/Domain/entities/Product.dart';
import '../../../Products/data/Model/ProductModel.dart';
import '../../Domain/entities/Orders.dart';
import '../model/OrdersModel.dart';

class OrderRemoteDataSourceImplemintation {
  String uID = dependecy<SharedPreferences>().getString('userId')!;
  Future<Unit?> add_Order(Orders order) async {
    var body = jsonEncode({
      "fullName": order.fullName,
      "address": order.address,
      "phoneNumber": order.phoneNumber,
      "total": order.total,
      "creatorId": uID,
      "products": order.products,
    });

    try {
      var response =
          await http.post(Uri.parse('$baseUrl/Orders.json'), body: body);
      return unit;
    } catch (e) {
      throw ServiceException();
    }
  }

  Future<List<Orders>> getOrders() async {
    try {
      var response = await http.get(Uri.parse(
          baseUrl + '/Orders.json?orderBy="creatorId"&equalTo="$uID"'));
      print(response.body);
      List<Orders> orders = [];
      (json.decode(response.body) as Map<String, dynamic>)?.forEach(
        (key, value) {
          print("helloForEach");
          orders.add(OrdersModel.fromJson(value, key));
        },
      );
      print(orders);

      return orders;
    } catch (e) {
      throw Exception(e);
      throw ServiceException();
    }
  }
}
