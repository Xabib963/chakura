// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class Orders extends Equatable {
  final String fullName;
  final String address;
  final String phoneNumber;
  final dynamic products;
  final double total;
  final String? id;
  final String? status;
  Orders({
    required this.status,
    this.id,
    required this.fullName,
    required this.address,
    required this.phoneNumber,
    required this.products,
    required this.total,
  });
  @override
  // TODO: implement props
  List<Object?> get props => [fullName, address, phoneNumber, products, total];
}
