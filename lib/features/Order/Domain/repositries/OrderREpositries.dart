import 'package:chakura/features/Order/presentation/page/Orders.dart';
import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Orders.dart';

abstract class OrderRepositry {
  Future<Either<Failure, Unit>> addOrder(Orders order);
  Future<Either<Failure, List<Orders>>> getOrders();
}
