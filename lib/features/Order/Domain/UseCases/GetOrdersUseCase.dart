import 'package:chakura/features/Order/Domain/entities/Orders.dart';
import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../repositries/OrderREpositries.dart';

class GetOrdersUseCase {
  final OrderRepositry orderRepositry;
  GetOrdersUseCase({
    required this.orderRepositry,
  });
  Future<Either<Failure, List<Orders>>> call() {
    return orderRepositry.getOrders();
  }
}
