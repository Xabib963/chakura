// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Orders.dart';
import '../repositries/OrderREpositries.dart';

class AddOrderUsecase {
  final OrderRepositry orderRepositry;
  AddOrderUsecase({
    required this.orderRepositry,
  });
  Future<Either<Failure, Unit>> call(Orders? order) {
    return orderRepositry.addOrder(order!);
  }
}
