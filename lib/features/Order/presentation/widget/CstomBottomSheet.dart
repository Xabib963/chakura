import 'package:chakura/features/Order/Domain/entities/Orders.dart';
import 'package:chakura/features/Order/presentation/page/Orders.dart';
import 'package:chakura/features/Order/presentation/widget/OrderInfo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/themes/Thems.dart';

class CustomBottomSheet extends StatelessWidget {
  const CustomBottomSheet({super.key, required this.order});
  final Orders order;
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.sizeOf(context);
    return Card(
      color: ColorTheme.mainLight,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(40))),
      child: Container(
          padding: EdgeInsets.all(15),
          height: media.height * 0.75,
          child: Column(
            children: [
              Divider(
                indent: media.width * 0.4,
                endIndent: media.width * 0.4,
                thickness: 3,
                color: ColorTheme.main,
              ),
              OrderInfo(
                content: order.fullName,
                inter: 'Full Name',
              ),
              OrderInfo(
                content: order.phoneNumber,
                inter: 'Phone Number',
              ),
              OrderInfo(
                content: order.address,
                inter: 'Address',
              ),
              Divider(
                indent: 30,
                endIndent: 30,
                color: ColorTheme.additional2,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Products :",
                  style: textStyle(
                      size: 18.sp,
                      color: ColorTheme.main,
                      weight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: media.height * 0.185,
                child: ListView.builder(
                  itemCount: order.products.length,
                  itemBuilder: (context, i) => ListTile(
                    subtitle: Text(
                      "${order.products[i]['price'].toString()} \$",
                      style: textStyle(
                          size: 16.sp,
                          color: Colors.grey,
                          weight: FontWeight.bold),
                    ),
                    title: Text(
                      order.products[i]['name'],
                      style: textStyle(
                          size: 18.sp,
                          color: ColorTheme.main,
                          weight: FontWeight.bold),
                    ),
                    trailing: Text('${order.products[i]['quantity']} QTY'),
                  ),
                ),
              )
            ],
          )),
    );
    ;
  }
}
