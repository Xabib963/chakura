import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../core/themes/Thems.dart';

class OrderInfo extends StatelessWidget {
  const OrderInfo({
    super.key,
    required this.inter,
    required this.content,
  });
  final String inter;
  final String content;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: SizedBox(
        width: double.infinity,
        child: RichText(
            text: TextSpan(children: [
          TextSpan(
              text: '$inter:  ',
              style: textStyle(
                  size: 18, color: ColorTheme.main, weight: FontWeight.normal)),
          TextSpan(
              text: content,
              style: textStyle(
                  size: 16, color: Colors.grey, weight: FontWeight.bold)),
        ])),
      ),
    );
  }
}
