// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'orders_bloc_bloc.dart';

abstract class OrdersEvent extends Equatable {
  const OrdersEvent();

  @override
  List<Object> get props => [];
}

class AddOrderEvent extends OrdersEvent {
  final Orders order;
  AddOrderEvent({
    required this.order,
  });
  @override
  List<Object> get props => [order];
}

class GetOrderEvent extends OrdersEvent {}
