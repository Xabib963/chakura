import 'package:bloc/bloc.dart';
import 'package:chakura/features/Order/Domain/UseCases/GetOrdersUseCase.dart';
import 'package:chakura/features/Order/presentation/page/Orders.dart';
import 'package:chakura/features/Products/Domain/usecases/AddToFvoritesUseCases.dart';
import 'package:equatable/equatable.dart';

import '../../../../../core/errors/failures.dart';
import '../../../Domain/UseCases/AddOrderUseCase.dart';
import '../../../Domain/entities/Orders.dart';

part 'orders_bloc_event.dart';
part 'orders_bloc_state.dart';

class OrdersBloc extends Bloc<OrdersEvent, OrdersState> {
  final AddOrderUsecase addOrderUsecase;
  final GetOrdersUseCase ordersUseCase;
  bool initialLoading = false;
  OrdersBloc(this.addOrderUsecase, this.ordersUseCase)
      : super(OrdersInitial()) {
    on<AddOrderEvent>((event, emit) async {
      initialLoading = true;
      emit(OrdersInitial());
      var doneORFailed = await addOrderUsecase(event.order);
      doneORFailed!.fold((l) {
        initialLoading = false;
        emit(OrderAddFailed(msg: _mapFailureTomessege(l!)));
      }, (r) {
        initialLoading = false;
        emit(OrderAdded(msg: "Your Order Added Successfully"));
      });
    });
    on<GetOrderEvent>((event, emit) async {
      emit(OrderLoading());
      var doneORFailed = await ordersUseCase();
      doneORFailed!.fold((l) {
        emit(OrderLoadingFailed(msg: _mapFailureTomessege(l!)));
      }, (r) {
        emit(OrderLoaded(r));
      });
    });
  }
  String _mapFailureTomessege(Failure fail) {
    switch (fail.runtimeType) {
      case ServicesFailure:
        return 'Cannt connect to data try again';
      case DataSourceFailure:
        return 'Something Went wrong';
      case OffLineFailure:
        return 'No Internet Connection';
      default:
        return "UnExcpected Error";
    }
  }
}
