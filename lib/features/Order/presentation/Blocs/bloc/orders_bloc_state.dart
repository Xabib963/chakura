// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'orders_bloc_bloc.dart';

abstract class OrdersState extends Equatable {
  const OrdersState();

  @override
  List<Object> get props => [];
}

class OrdersInitial extends OrdersState {}

class OrderAdded extends OrdersState {
  final String msg;
  OrderAdded({
    required this.msg,
  });
  @override
  List<Object> get props => [msg];
}

class OrderAddFailed extends OrdersState {
  final String msg;
  OrderAddFailed({
    required this.msg,
  });
  @override
  List<Object> get props => [msg];
}

class OrderLoading extends OrdersState {}

class OrderLoaded extends OrdersState {
  final List<Orders> orders;

  OrderLoaded(this.orders);
  @override
  List<Object> get props => [];
}

class OrderLoadingFailed extends OrdersState {
  final String msg;
  OrderLoadingFailed({
    required this.msg,
  });
}
