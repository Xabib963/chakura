import 'package:chakura/features/Order/data/model/OrdersModel.dart';
import 'package:chakura/features/Order/presentation/Blocs/bloc/orders_bloc_bloc.dart';
import 'package:chakura/features/Order/presentation/widget/CstomBottomSheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:sidebarx/sidebarx.dart';

import '../../../../Widgets/SideBarXCustom.dart';
import '../../../../core/themes/Thems.dart';
import '../widget/OrderInfo.dart';

class Order extends StatefulWidget {
  const Order({super.key});
  static const routeName = '/Order';
  @override
  State<Order> createState() => _OrderState();
}

class _OrderState extends State<Order> {
  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'O R D E R S ',
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontFamily: 'Poppins',
                color: Colors.white,
                fontSize: 25.sp,
                fontWeight: FontWeight.w500,
              ),
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new, color: Colors.white),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: SafeArea(
          child: BlocBuilder(
              bloc: context.read<OrdersBloc>()..add(GetOrderEvent()),
              builder: (context, state) {
                if (state is OrderLoading) {
                  return const Center(
                    child: CircularProgressIndicator(color: Colors.white),
                  );
                }
                if (state is OrderLoaded) {
                  return SizedBox(
                    height: media.height * 0.8,
                    child: ListView.builder(
                        itemCount: state.orders.length,
                        itemBuilder: (context, index) {
                          return Card(
                            color: Colors.transparent,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Container(
                              constraints: BoxConstraints(
                                  minWidth: media.width * 0.9,
                                  maxWidth: media.width * 0.9,
                                  maxHeight: media.height * 0.25,
                                  minHeight: media.height * 0.25),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  gradient: LinearGradient(colors: [
                                    ColorTheme.mainLight,
                                    ColorTheme.main
                                  ])),
                              child: LayoutBuilder(
                                  builder: (context, constrainet) {
                                return Stack(children: [
                                  Positioned(
                                      left: constrainet.maxWidth * 0.05,
                                      top: constrainet.maxHeight * 0.08,
                                      width: constrainet.maxWidth * 0.5,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          RichText(
                                            text: TextSpan(
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 21.sp),
                                                text:
                                                    "${state.orders[index].products.map((e) => e['name'])}"),
                                          ),
                                          TextButton(
                                              onPressed: () {
                                                showModalBottomSheet(
                                                    isDismissible: true,
                                                    backgroundColor:
                                                        Colors.transparent,
                                                    enableDrag: true,
                                                    barrierColor:
                                                        Colors.transparent,
                                                    context: context,
                                                    builder: (context) {
                                                      return CustomBottomSheet(
                                                          order: state
                                                              .orders[index]);
                                                    });
                                              },
                                              child: Text(
                                                "view deitels",
                                                style: TextStyle(
                                                    color: ColorTheme.mainOpa,
                                                    fontSize: 15.sp),
                                              ))
                                        ],
                                      )),
                                  Positioned(
                                      right: constrainet.maxWidth * 0.001,
                                      child: Image.asset(
                                          width: constrainet.maxWidth * 0.4,
                                          'assets/Shopping bag-amico.png')),
                                  Positioned(
                                    width: constrainet.maxWidth,
                                    bottom: media.height * 0.001,
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Row(
                                            children: [
                                              Image.asset(
                                                width: constrainet.minWidth / 8,
                                                // height: constrainet.,
                                                'assets/1495815224-jd15_84582.png',
                                              ),
                                              SizedBox(
                                                width:
                                                    constrainet.minWidth / 6.3,
                                                child: Text(
                                                  'Master',
                                                  style: TextStyle(
                                                      fontSize: 12.sp,
                                                      color: Colors.white),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                          child: Row(
                                            children: [
                                              Icon(
                                                size: constrainet.minWidth / 16,
                                                Icons.done_all,
                                                color: Colors.green,
                                              ),
                                              SizedBox(
                                                width:
                                                    constrainet.minWidth / 4.3,
                                                child: Text(
                                                  state.orders[index].status ==
                                                          null
                                                      ? ' Pending'
                                                      : '   ${state.orders[index].status}',
                                                  style: TextStyle(
                                                      fontSize: 12.sp,
                                                      color: Colors.white),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                          child: Row(
                                            children: [
                                              Icon(
                                                size: constrainet.minWidth / 16,
                                                Icons.monetization_on_outlined,
                                                color: Colors.green,
                                              ),
                                              SizedBox(
                                                width:
                                                    constrainet.minWidth / 5.0,
                                                child: Text(
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  '${state.orders[index].total}\$',
                                                  style: TextStyle(
                                                    fontSize: 12.sp,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ]);
                              }),
                            ),
                          );
                        }),
                  );
                } else {
                  return SizedBox();
                }
              })),
    );
  }
}
