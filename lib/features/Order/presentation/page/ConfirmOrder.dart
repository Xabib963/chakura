import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:chakura/core/themes/Thems.dart';
import 'package:chakura/features/Order/presentation/Blocs/bloc/orders_bloc_bloc.dart';
import 'package:chakura/features/Order/presentation/page/Orders.dart';
import 'package:chakura/features/Products/Domain/entities/CartProduct.dart';
import 'package:chakura/features/Products/Domain/entities/Product.dart';
import 'package:chakura/features/Products/presentation/Blocs/bloc/get_add_up_delete_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../Products/presentation/Widget/Custominput.dart';
import '../../Domain/entities/Orders.dart';

class ConfirmOrder extends StatelessWidget {
  static const routeName = "/ConfirmOrder";
  ConfirmOrder({super.key});
  double total = 0;
  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController brandController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  String imgurl = '';

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> arg =
        ModalRoute.of(context)!.settings!.arguments as Map<String, dynamic>;
    List<CartProduct> ls = arg['ls'];
    double total = arg['total'];
    Size media = MediaQuery.sizeOf(context);
    bool initialLoading = false;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_new,
              color: Colors.white,
            ),
            onPressed: () => Navigator.pop(context)),
        backgroundColor: Colors.transparent,
        title: const Text(
          'C O N F I R M',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
          child: Form(
        key: formKey,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: media.height * 0.2,
                child: ListView.builder(
                  itemCount: ls.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.all(8.0.w),
                      child: ListTile(
                        leading: CircleAvatar(
                            radius: 40.r,
                            backgroundImage: NetworkImage(ls[index].img)),
                        title: Text(
                          ls[index].pName,
                          style: textStyle(
                              size: 20.sp,
                              color: Colors.white,
                              weight: FontWeight.bold),
                        ),
                        subtitle: Text(
                          ls[index].brand,
                          style: textStyle(
                              size: 16.sp,
                              color: Colors.white70,
                              weight: FontWeight.normal),
                        ),
                        trailing: Text(
                          ls[index].quantity.toString(),
                          style: textStyle(
                              size: 18.sp,
                              color: Colors.white,
                              weight: FontWeight.bold),
                        ),
                      ),
                    );
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(15.0.w),
                child: SingleChildScrollView(
                  child: Column(children: [
                    CustomInput(
                      number: false,
                      controller: nameController,
                      hint: "Full Name",
                      title: "name",
                    ),
                    SizedBox(
                      height: media.height * 0.01,
                    ),
                    CustomInput(
                      number: false,
                      controller: addressController,
                      hint: "Address",
                      title: "Address",
                    ),
                    SizedBox(
                      height: media.height * 0.01,
                    ),
                    CustomInput(
                      number: true,
                      controller: phoneController,
                      hint: "Phone Number ",
                      title: "Phone",
                    ),
                    SizedBox(
                      height: media.height * 0.01,
                    ),
                    StatefulBuilder(builder: (context, setState) {
                      return BlocListener<OrdersBloc, OrdersState>(
                          listener: (context, state) {
                            if (state is OrderAdded) {
                              setState(
                                () {},
                              );
                              final snackBar = SnackBar(
                                elevation: 0,
                                behavior: SnackBarBehavior.floating,
                                backgroundColor: Colors.transparent,
                                content: AwesomeSnackbarContent(
                                  title: 'Congrats !!',
                                  message: state.msg,

                                  /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
                                  contentType: ContentType.success,
                                ),
                              );

                              ScaffoldMessenger.of(context)
                                ..hideCurrentSnackBar()
                                ..showSnackBar(snackBar);
                            }
                            if (state is OrderAddFailed) {
                              setState(
                                () {},
                              );
                              final snackBar = SnackBar(
                                elevation: 0,
                                behavior: SnackBarBehavior.floating,
                                backgroundColor: Colors.transparent,
                                content: AwesomeSnackbarContent(
                                  title: 'On Snap!',
                                  message: state.msg,

                                  /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
                                  contentType: ContentType.failure,
                                ),
                              );

                              ScaffoldMessenger.of(context)
                                ..hideCurrentSnackBar()
                                ..showSnackBar(snackBar);
                            }
                          },
                          child: context.read<OrdersBloc>().initialLoading
                              ? Center(
                                  child: CircularProgressIndicator(
                                      color: ColorTheme.mainOpa),
                                )
                              : Padding(
                                  padding: EdgeInsets.symmetric(vertical: 30.w),
                                  child: InkWell(
                                    onTap: () async {
                                      if (formKey.currentState!.validate()) {
                                        context.read<OrdersBloc>().add(
                                            AddOrderEvent(
                                                order: Orders(
                                                    status: 'Pending',
                                                    address:
                                                        addressController.text,
                                                    fullName:
                                                        nameController.text,
                                                    phoneNumber:
                                                        phoneController.text,
                                                    total: total,
                                                    products: ls
                                                        .map((e) => {
                                                              "id": e.id,
                                                              "name": e.pName,
                                                              "price":
                                                                  e.totalPrice,
                                                              "quantity":
                                                                  e.quantity
                                                            })
                                                        .toList())));
                                        setState(
                                          () {},
                                        );
                                      }
                                    },
                                    child: Container(
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(15.r)),
                                        width: media.width * 0.4,
                                        height: media.width * 0.12,
                                        child: Text(
                                          "Send",
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyLarge!
                                              .copyWith(
                                                  color: ColorTheme.main,
                                                  fontSize: 23.sp),
                                        )),
                                  ),
                                ));
                    })
                  ]),
                ),
              ),
              Divider(
                color: Colors.white,
                endIndent: 50.w,
                thickness: 1,
                indent: 50.w,
              ),
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8.0, vertical: 50),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Total : $total \$",
                          style: textStyle(
                              size: 25.sp,
                              color: Colors.white,
                              weight: FontWeight.bold),
                        )
                      ]),
                ),
              )
            ],
          ),
        ),
      )),
    );
  }
}
