// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:chakura/core/errors/Exception.dart';
import 'package:chakura/features/Profile/data/model/OwnUserModel.dart';
import 'package:dartz/dartz.dart';

import 'package:chakura/core/errors/failures.dart';
import 'package:chakura/features/Profile/domain/model/OwnUser.dart';

import '../../../../core/Network/connection_info.dart';
import '../../domain/repositories/getUserRepos.dart';
import '../DataProviders/GetUserApi.dart';

class GetUserReposImplimetation implements GetUserRepos {
  final NetworkInfo networkInfo;
  final GetUserApi _getUserApi;
  GetUserReposImplimetation(
    this._getUserApi, {
    required this.networkInfo,
  });
  @override
  Future<Either<Failure, OwnUser>> getUserProfile() async {
    if (await networkInfo.isConnected) {
      try {
        OwnUserModel user = await _getUserApi.getUSer();
        return Right(user);
      } on ServiceException {
        return Left(ServicesFailure());
      }
    } else {
      // if i had a local data i need to try get products if null throw error
      return Left(OffLineFailure());
    }
  }
}
