import '../../domain/model/OwnUser.dart';

class OwnUserModel extends OwnUser {
  OwnUserModel(
      {required super.address,
      required super.age,
      required super.email,
      required super.img,
      required super.job,
      required super.name,
      required super.password,
      required super.phone,
      required super.userId});

  factory OwnUserModel.fromJson(Map<String, dynamic> json) {
    return OwnUserModel(
      address: json['address'],
      age: json['age'],
      email: json['email'],
      img: json['img'],
      job: json['job'],
      name: json['name'],
      password: json['password'],
      phone: json['phone'],
      userId: json['userId'],
    );
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address'] = super.address;
    data['age'] = super.age;
    data['email'] = super.email;
    data['img'] = super.img;
    data['job'] = super.job;
    data['name'] = super.name;
    data['password'] = super.password;
    data['phone'] = super.phone;
    data['userId'] = super.userId;
    return data;
  }
}
