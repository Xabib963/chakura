import 'dart:convert';

import 'package:chakura/InjectionContainer.dart';
import 'package:chakura/core/Constants.dart';
import 'package:chakura/features/Profile/data/model/OwnUserModel.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/errors/Exception.dart';

class GetUserApi {
  String uID = dependecy<SharedPreferences>().getString('userId')!;
  Future<OwnUserModel> getUSer() async {
    try {
      List<OwnUserModel> user = [];
      var response = await http.get(
          Uri.parse(baseUrl + '/Users.json?orderBy="userId"&equalTo="$uID"'));
      (json.decode(response.body) as Map<String, dynamic>)?.forEach(
        (key, value) {
          user.add(OwnUserModel.fromJson(value));
        },
      );
      dependecy<SharedPreferences>().setString('img', user.first.img!);
      return user.first;
    } catch (e) {
      print(e);
      throw ServiceException();
    }
  }
}
