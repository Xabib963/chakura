class OwnUser {
  String? address;
  String? age;
  String? email;
  String? img;
  String? job;
  String? name;
  String? password;
  String? phone;
  String? userId;

  OwnUser(
      {required this.address,
      required this.age,
      required this.email,
      required this.img,
      required this.job,
      required this.name,
      required this.password,
      required this.phone,
      required this.userId});
}
