import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../model/OwnUser.dart';
import '../repositories/getUserRepos.dart';

class GetProfileInfoUseCases {
  final GetUserRepos _getUserRepos;

  GetProfileInfoUseCases(this._getUserRepos);

  Future<Either<Failure, OwnUser>> call() {
    return _getUserRepos.getUserProfile();
  }
}
