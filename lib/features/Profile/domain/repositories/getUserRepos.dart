import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../model/OwnUser.dart';

abstract class GetUserRepos {
  Future<Either<Failure, OwnUser>> getUserProfile();
}
