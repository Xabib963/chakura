import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HiligtedContainer extends StatelessWidget {
  const HiligtedContainer({
    super.key,
    required this.icon,
    required this.color,
    required this.data,
  });
  final IconData icon;
  final Color color;
  final String data;

  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.of(context).size;
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.r),
              color: color.withOpacity(0.5)),
          width: double.infinity,
          alignment: Alignment.center,
          height: media.height * 0.12,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            maxLines: 1,
            " ${data}",
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                fontWeight: FontWeight.bold,
                fontSize: 25.sp,
                color: Colors.white),
          ),
        ),
        Positioned(
            top: -10.w,
            left: 20.w,
            child: Icon(
              icon,
              color: Colors.white,
              size: 30.sp,
            )),
      ],
    );
  }
}
