import 'package:chakura/core/themes/Thems.dart';
import 'package:chakura/features/Profile/presintation/bloc/user_profile_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../InjectionContainer.dart';
import '../widgets/HilightedContainer.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({super.key});
  static const routeName = '/profileScreen';
  // DatabaseServices _dataSource = DatabaseServices();
  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.center,
            child: CircleAvatar(
              backgroundColor: ColorTheme.secondary,
              radius: 90.r,
              child: CircleAvatar(
                radius: 85.r,
                backgroundImage: AssetImage(
                  'assets/${dependecy<SharedPreferences>().getString('img') ?? 'batman.png'}',
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0.w),
            child: SizedBox(
              height: media.height * 0.57,
              child: BlocBuilder<UserProfileBloc, UserProfileState>(
                  bloc: context.read<UserProfileBloc>()
                    ..add(GetUSerProfileEvent()),
                  builder: (context, state) {
                    if (state is UserProfileInitial) {
                      return const Center(
                        child: CircularProgressIndicator(color: Colors.white),
                      );
                    }
                    if (state is UserProfileLoaded) {
                      return ListView(
                        padding: EdgeInsets.symmetric(horizontal: 15.w),
                        shrinkWrap: true,
                        children: [
                          Text(
                            "USER Details :",
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge!
                                .copyWith(fontSize: 20.sp, color: Colors.white),
                          ),
                          Divider(
                            thickness: 3,
                            endIndent: 10,
                            indent: 5,
                            color: ColorTheme.secondary,
                          ),
                          SizedBox(
                            height: media.height * 0.03,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: ColorTheme.secondary.withOpacity(0.5)),
                            width: double.infinity,
                            height: media.height * 0.22,
                            padding: EdgeInsets.symmetric(horizontal: 10.w),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: media.height * 0.01,
                                  ),
                                  Text(
                                    "Name : ",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyLarge!
                                        .copyWith(
                                          color: Colors.white,
                                          fontSize: 18.sp,
                                          fontWeight: FontWeight.bold,
                                        ),
                                  ),
                                  Text(
                                    state.user.name!.toUpperCase(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyLarge!
                                        .copyWith(
                                          color: Colors.white,
                                          fontSize: 22.sp,
                                          fontWeight: FontWeight.bold,
                                        ),
                                  ),
                                  SizedBox(
                                    height: media.height * 0.01,
                                  ),
                                  Text(
                                    "User_Id :${state.user.userId!}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyLarge!
                                        .copyWith(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12.sp,
                                            color: Colors.white),
                                  ),
                                ]),
                          ),
                          SizedBox(
                            height: media.height * 0.03,
                          ),
                          HiligtedContainer(
                            color: ColorTheme.mainLight,
                            data: state.user.email!,
                            icon: Icons.email,
                          ),
                          SizedBox(
                            height: media.height * 0.02,
                          ),
                          HiligtedContainer(
                            color: Colors.indigo[600]!,
                            data: state.user.address!,
                            icon: Icons.home,
                          ),
                          SizedBox(
                            height: media.height * 0.02,
                          ),
                          HiligtedContainer(
                            color: Colors.indigo[400]!,
                            data: state.user.phone!,
                            icon: Icons.phone,
                          ),
                          SizedBox(
                            height: media.height * 0.03,
                          ),
                          SizedBox(
                            width: media.width,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Stack(
                                    clipBehavior: Clip.none,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15.r),
                                            color: Colors.deepPurple
                                                .withOpacity(0.3)),
                                        width: double.infinity,
                                        alignment: Alignment.center,
                                        height: media.height * 0.12,
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10),
                                        child: FittedBox(
                                          child: Text(
                                            maxLines: 1,
                                            state.user.job!,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyLarge!
                                                .copyWith(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 20.sp,
                                                    color: Colors.white),
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        top: -15.w,
                                        left: 15.w,
                                        child: Text(
                                          maxLines: 1,
                                          " job",
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyLarge!
                                              .copyWith(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20.sp,
                                                  color: Colors.white),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: 10.w,
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Stack(
                                    clipBehavior: Clip.none,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15.r),
                                            color: Colors.deepPurple!
                                                .withOpacity(0.5)),
                                        width: double.infinity,
                                        alignment: Alignment.center,
                                        height: media.height * 0.12,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.w),
                                        child: Text(
                                          maxLines: 1,
                                          state.user.age!,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyLarge!
                                              .copyWith(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20.w,
                                                  color: Colors.white),
                                        ),
                                      ),
                                      Positioned(
                                        top: -15.w,
                                        left: 15.w,
                                        child: Text(
                                          maxLines: 1,
                                          " Age",
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyLarge!
                                              .copyWith(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20.sp,
                                                  color: Colors.white),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: media.height * 0.005,
                          ),
                        ],
                      );
                    }
                    if (state is UserProfileError) {
                      return Text(state.msg,
                          style: const TextStyle(color: Colors.red));
                    }
                    return const SizedBox();
                  }),
            ),
          )
        ],
      )),
      appBar: AppBar(
        scrolledUnderElevation: 0,
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new, color: Colors.white),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Colors.transparent,
        title: Text(
          'Profile',
          style: Theme.of(context)
              .textTheme
              .bodyLarge!
              .copyWith(color: Colors.white, fontSize: 30),
        ),
        centerTitle: true,
      ),
    );
  }
}
