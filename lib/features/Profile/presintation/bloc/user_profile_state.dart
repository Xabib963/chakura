// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'user_profile_bloc.dart';

sealed class UserProfileState extends Equatable {
  const UserProfileState();

  @override
  List<Object> get props => [];
}

final class UserProfileInitial extends UserProfileState {}

class UserProfileLoaded extends UserProfileState {
  final OwnUser user;

  UserProfileLoaded(this.user);
}

class UserProfileError extends UserProfileState {
  final String msg;
  UserProfileError({
    required this.msg,
  });
}
