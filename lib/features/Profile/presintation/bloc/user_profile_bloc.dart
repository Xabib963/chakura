import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/errors/failures.dart';
import '../../domain/UseCases/GetUserInfoUsecase.dart';
import '../../domain/model/OwnUser.dart';

part 'user_profile_event.dart';
part 'user_profile_state.dart';

class UserProfileBloc extends Bloc<UserProfileEvent, UserProfileState> {
  final GetProfileInfoUseCases _getProfileInfoUseCases;
  UserProfileBloc(this._getProfileInfoUseCases) : super(UserProfileInitial()) {
    on<UserProfileEvent>((event, emit) async {
      var user = await _getProfileInfoUseCases();
      print(user);
      user.fold((l) => emit(UserProfileError(msg: _mapFailureTomessege(l))),
          (r) => emit(UserProfileLoaded(r)));
    });
  }
  String _mapFailureTomessege(Failure fail) {
    switch (fail.runtimeType) {
      case ServicesFailure:
        return '';
      case DataSourceFailure:
        return '';
      case OffLineFailure:
        return 'please Check your connection';
      default:
        return "UnEcpected Error";
    }
  }
}
