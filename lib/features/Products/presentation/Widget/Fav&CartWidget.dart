import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/themes/Thems.dart';
import '../../Domain/entities/CartProduct.dart';
import '../../Domain/entities/Product.dart';
import '../../data/DataProviders/LocalDB.dart';
import '../Blocs/bloc/addto_hive_bloc.dart';
import '../Cubit/cubit/quantity_cubit_cubit.dart';

class FavAndCartWidget extends StatelessWidget {
  const FavAndCartWidget({
    super.key,
    required this.media,
    this.pro,
    this.cart,
    required this.isCart,
  });
  final Product? pro;
  final CartProduct? cart;
  final Size media;
  final bool isCart;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: media.width * 0.9,
      height: media.height * 0.18,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.r),
        color: ColorTheme.mainLight.withOpacity(0.7),
      ),
      margin: EdgeInsets.all(10.w),
      child: Stack(clipBehavior: Clip.none, children: [
        Positioned(
          top: -media.height * 0.017,
          left: -media.width * 0.03,
          child: Image.network(
            height: media.height * 0.2,
            width: media.width * 0.3,
            pro == null ? cart!.img : pro!.img,
            errorBuilder: (context, error, stackTrace) => const Icon(
              Icons.error,
              color: Colors.white,
            ),
          ),
        ),
        Positioned(
            width: 50.w,
            top: 15.w,
            right: 20.w,
            child: FittedBox(
              child: RichText(
                text: TextSpan(children: [
                  TextSpan(
                    text: pro == null
                        ? cart!.totalPrice.toString()
                        : pro!.price.toString(),
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 18.sp,
                    ),
                  ),
                  TextSpan(
                    text: ' \$',
                    style: TextStyle(
                      color: Color.fromARGB(255, 3, 173, 26),
                      fontWeight: FontWeight.w600,
                      fontSize: 18.sp,
                    ),
                  ),
                ]),
              ),
            )),
        Positioned(
            width: media.width * 0.4,
            top: media.height * 0.03,
            right: media.width * 0.28,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: media.width * 0.3,
                  child: FittedBox(
                    child: RichText(
                      maxLines: 1,
                      text: TextSpan(children: [
                        TextSpan(
                          text: '${pro == null ? cart!.pName : pro!.pName} ',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 10.sp,
                          ),
                        ),
                      ]),
                    ),
                  ),
                ),
                SizedBox(
                  width: media.width * 0.15,
                  child: FittedBox(
                    child: RichText(
                      maxLines: 1,
                      text: TextSpan(children: [
                        TextSpan(
                          text: pro == null ? cart!.brand : pro!.brand,
                          style: TextStyle(
                            color: Color.fromARGB(255, 133, 133, 133),
                            fontSize: 33.sp,
                          ),
                        )
                      ]),
                    ),
                  ),
                )
              ],
            )),
        Positioned(
            top: isCart ? media.height * 0.1 : media.height * 0.08,
            right: isCart ? media.width * 0.05 : media.width * 0.1,
            child: isCart
                ? Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.r),
                      color: ColorTheme.mainOpa.withOpacity(0.3),
                    ),
                    width: 90.w,
                    height: media.height * 0.05,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () {
                            context
                                .read<QuantityCubitCubit>()
                                .updateQuantityMines(cart!);
                          },
                          child: Card(
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.r)),
                            child: Center(
                                child: Icon(
                              Icons.remove,
                              size: 30.sp,
                            )),
                          ),
                        ),
                        BlocBuilder<QuantityCubitCubit, int>(
                          builder: (context, state) {
                            return Center(
                              child: SizedBox(
                                  width: 10.w,
                                  child: Text(
                                    cart!.quantity.toString(),
                                    style: TextStyle(
                                        fontSize: 15.sp,
                                        color: Colors.grey,
                                        fontWeight: FontWeight.bold),
                                  )),
                            );
                          },
                        ),
                        InkWell(
                          onTap: () {
                            context
                                .read<QuantityCubitCubit>()
                                .updateQuantityAdd(cart!);
                          },
                          child: Card(
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.r)),
                            child: Center(
                                child: Icon(
                              Icons.add,
                              size: 30.sp,
                            )),
                          ),
                        )
                      ],
                    ),
                  )
                : InkWell(
                    onTap: () {
                      context.read<AddtoHiveBloc>().add(
                          RemoveFromFavoritesLocalEvent(
                              pro: pro!,
                              box: LocalDB.instance.getFavoritesBox()));
                      Future.delayed(const Duration(seconds: 1));
                      context.read<AddtoHiveBloc>().add(
                          GetItems(box: LocalDB.instance.getFavoritesBox()));
                    },
                    child: CircleAvatar(
                      radius: media.width * 0.07,
                      backgroundColor: Colors.white,
                      child: Icon(
                        Icons.delete_outline_outlined,
                        size: 25.sp,
                        color: Colors.red.shade800,
                      ),
                    ),
                  )),
      ]),
    );
  }
}
