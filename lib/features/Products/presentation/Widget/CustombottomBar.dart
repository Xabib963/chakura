import 'package:chakura/features/Products/presentation/Cubit/cubit/quantity_cubit_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:sidebarx/sidebarx.dart';
import '../../../../InjectionContainer.dart';
import '../../../../core/themes/Thems.dart';
import '../Blocs/bloc/addto_hive_bloc.dart';

import '../Blocs/bloc/get_add_up_delete_bloc.dart';
import '../Cubit/Navigationcubit/navidation_cubit.dart';
import '../Page/Cart.dart';
import '../Page/Equipments.dart';
import '../../../Order/presentation/page/Orders.dart';
import '../../../../Home.dart';
import '../../../GetIn/presentaion/cubit/add_tofavorites_cubit.dart';
import '../../../GetIn/presentaion/cubit/counter_cubit.dart';

import '../../../GetIn/presentaion/cubit/categuries_cubit_cubit.dart';
import '../Page/Faveorits.dart';
import '../../../../Widgets/SideBarXCustom.dart';

class CustomBottomBar extends StatelessWidget {
  CustomBottomBar({super.key});
  static const routeName = '/landing';
  final _controller = SidebarXController(selectedIndex: 0, extended: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            iconTheme: const IconThemeData(color: Colors.white),
            elevation: 0,
            centerTitle: true,
            backgroundColor: Colors.transparent,
            title: Text(
              'C h a k u r a',
              style: textStyle(
                  color: Colors.white, size: 20, weight: FontWeight.bold),
            )),
        drawer: SideBarXWidget(controller: _controller),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: BlocBuilder<NavidationCubit, NavidationState>(
          builder: (context, state) {
            return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: ColorTheme.secondary,
                ),
                padding: EdgeInsets.all(15),
                margin: EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    InkWell(
                      onTap: () {
                        context.read<NavidationCubit>().homeLand();
                      },
                      child: CircleAvatar(
                        backgroundColor: state is NavidationInitial
                            ? Colors.white
                            : Colors.transparent,
                        child: Icon(
                          Icons.home,
                          color: state is NavidationInitial
                              ? ColorTheme.mainLight
                              : Colors.white,
                        ),
                      ),
                    ),
                    InkWell(
                        onTap: () {
                          context.read<NavidationCubit>().favoriteLand();
                        },
                        child: CircleAvatar(
                          backgroundColor: state is NavidationFav
                              ? Colors.white
                              : Colors.transparent,
                          child: Icon(Icons.favorite,
                              color: state is NavidationFav
                                  ? ColorTheme.mainLight
                                  : Colors.white),
                        )),
                    InkWell(
                        onTap: () {
                          context.read<NavidationCubit>().cartLand();
                        },
                        child: CircleAvatar(
                            backgroundColor: state is NavidationCart
                                ? Colors.white
                                : Colors.transparent,
                            child: Icon(Icons.payments,
                                color: state is NavidationCart
                                    ? ColorTheme.mainLight
                                    : Colors.white))),
                  ],
                ));
          },
        ),
        body: BlocBuilder<NavidationCubit, NavidationState>(
          bloc: context.read<NavidationCubit>(),
          builder: (context, state) {
            if (state is NavidationInitial) {
              return MultiBlocProvider(
                providers: [
                  BlocProvider.value(value: context.read<GetAddUpDeleteBloc>()),
                ],
                child: Equipments(),
              );
            }
            if (state is NavidationFav) {
              return MultiBlocProvider(providers: [
                BlocProvider.value(value: context.read<AddtoHiveBloc>()),
              ], child: Favorites());
            }
            if (state is NavidationCart) {
              return MultiBlocProvider(
                providers: [
                  BlocProvider<QuantityCubitCubit>(
                    create: (context) => QuantityCubitCubit(),
                  ),
                  BlocProvider.value(
                    value: context.read<AddtoHiveBloc>(),
                  )
                ],
                child: Cart(),
              );
            }
            return SizedBox();
          },
        ));
  }
}
