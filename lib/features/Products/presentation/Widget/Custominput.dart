import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomInput extends StatelessWidget {
  const CustomInput({
    super.key,
    required this.hint,
    required this.title,
    required this.controller,
    required this.number,
  });
  final String hint;
  final String title;
  final bool number;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.sizeOf(context);
    return SizedBox(
      width: media.width * 0.9,
      height: media.height * 0.061,
      child: TextFormField(
        validator: (value) {
          if (value!.length < 2) {
            return "too short input";
          }

          return null;
        },
        controller: controller,
        style: TextStyle(color: Colors.white),
        textAlign: TextAlign.start,
        inputFormatters: number
            ? <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
              ]
            : [],
        keyboardType: number ? TextInputType.number : null,
        textAlignVertical: TextAlignVertical.center,
        decoration: InputDecoration(
          hintStyle: TextStyle(color: Colors.white, fontSize: 18.sp),
          hintText: hint,
          alignLabelWithHint: true,
          focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white)),
          disabledBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white)),
          enabledBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white10)),
        ),
      ),
    );
  }
}
