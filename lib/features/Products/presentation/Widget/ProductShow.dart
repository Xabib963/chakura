import 'package:chakura/features/Products/Domain/entities/Product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../core/themes/Thems.dart';
import '../Page/EquipmentDiatels.dart';

class ProductShow extends StatelessWidget {
  const ProductShow({super.key, required this.product});
  final Product product;
  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.sizeOf(context);
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .pushNamed(EquipmentDiatels.routeName, arguments: product);
      },
      child: Container(
        height: media.height * 0.38,
        margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.w),
        width: media.width * 0.55,
        child: Card(
            color: ColorTheme.mainLight.withOpacity(0.7),
            semanticContainer: true,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  clipBehavior: Clip.none,
                  fit: StackFit.passthrough,
                  children: [
                    RotatedBox(
                      quarterTurns: 5,
                      child: SizedBox(
                        width: media.width * 0.5,
                        child: FittedBox(
                          child: Text(
                            product.brand.toString(),
                            style: TextStyle(
                              color: Colors.grey.withOpacity(0.5),
                              fontWeight: FontWeight.w600,
                              fontSize: 35.sp,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                        top: -23.w,
                        left: 90.w,
                        child: Image.network(
                            errorBuilder: (context, error, stackTrace) =>
                                const Icon(
                                  Icons.error_outline,
                                  color: Colors.white,
                                ),
                            height: media.height * 0.32,
                            width: media.width * 0.35,
                            product.img.toString()))
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 8.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        product.pName.toString(),
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 23.sp,
                        ),
                      ),
                      Text(
                        product.type,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Colors.grey.withOpacity(0.5),
                          fontWeight: FontWeight.w600,
                          fontSize: 15.sp,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0.w),
                  child: SizedBox(
                    width: media.width * 0.55,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(
                            '${product.price}\$',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 18.sp,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              FaIcon(
                                Icons.star,
                                size: 24.sp,
                                color: Colors.amber,
                              ),
                              SizedBox(
                                width: media.width * 0.12,
                                child: Text(
                                  '${product.rate}',
                                  style: TextStyle(
                                    color: Colors.amber,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18.sp,
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }
}
