import 'package:chakura/features/Products/presentation/Cubit/ChooseImage/choose_image_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomButtom extends StatelessWidget {
  CustomButtom({
    super.key,
    required this.msg,
  });
  final String msg;

  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.sizeOf(context);
    return BlocProvider<ChooseImageCubit>(
      create: (context) => ChooseImageCubit(),
      child: Expanded(
        flex: 1,
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.symmetric(
              horizontal: media.width * 0.05, vertical: media.height * 0.01),
          child: InkWell(
            onTap: () async {
              context.read<ChooseImageCubit>().chooseImage(context);
            },
            child: Card(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 22.0),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                          width: media.width * 0.6,
                          child: FittedBox(
                            child: Text(
                              msg,
                              maxLines: 1,
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineLarge!
                                  .copyWith(fontSize: 28),
                            ),
                          )),
                      SizedBox(
                          width: media.width * 0.1,
                          child: Icon(
                            Icons.add_circle,
                            color: Colors.grey.shade600,
                            size: media.width * 0.08,
                          ))
                    ]),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
