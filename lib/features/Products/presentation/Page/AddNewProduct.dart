import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:chakura/InjectionContainer.dart';
import 'package:chakura/core/themes/Thems.dart';
import 'package:chakura/features/Products/Domain/entities/Product.dart';
import 'package:chakura/features/Products/presentation/Blocs/bloc/get_add_up_delete_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Cubit/chooseTypeCubit.dart/cubit/choose_type_cubit.dart';
import '../Widget/Custombutton.dart';
import '../Widget/Custominput.dart';

class AddProductScreen extends StatefulWidget {
  AddProductScreen({super.key, required this.title, required this.value});
  final String? title;
  final Product? value;
  static const routeName = '/addProduct';

  @override
  State<AddProductScreen> createState() => _AddProductScreenState();
}

class _AddProductScreenState extends State<AddProductScreen> {
  TextEditingController nameController = TextEditingController();

  TextEditingController descriptionController = TextEditingController();

  TextEditingController priceController = TextEditingController();

  TextEditingController brandController = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  String imgurl = '';

  @override
  void initState() {
    if (widget.value != null) {
      nameController.text = widget.value!.pName;
      descriptionController.text = widget.value!.description;
      priceController.text = widget.value!.price.toString();
      brandController.text = widget.value!.brand;
      imgurl = widget.value!.img;
    } else {}
    super.initState();
  }

  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.sizeOf(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios_new,
              color: Colors.white,
            ),
            onPressed: () => Navigator.pop(context)),
        backgroundColor: Colors.transparent,
        title: Text(
          widget.title!,
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: BlocListener<GetAddUpDeleteBloc, GetAddUpDeleteState>(
        bloc: context.read<GetAddUpDeleteBloc>(),
        listener: (context, state) {
          if (state is AddUpDeleteMessegeState) {
            isLoading = false;
            final snackBar = SnackBar(
              elevation: 0,
              behavior: SnackBarBehavior.floating,
              backgroundColor: Colors.transparent,
              content: AwesomeSnackbarContent(
                title: 'Congrats!',
                message: state.messege,

                /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
                contentType: ContentType.success,
              ),
            );

            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(snackBar);
            setState(() {});
          }
          if (state is AddUpDeleteErrorMessegeState) {
            isLoading = false;
            final snackBar = SnackBar(
              elevation: 0,
              behavior: SnackBarBehavior.floating,
              backgroundColor: Colors.transparent,
              content: AwesomeSnackbarContent(
                title: 'Oh Snap!',
                message: state.messege,

                /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
                contentType: ContentType.failure,
              ),
            );

            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(snackBar);
            setState(() {});
          }
        },
        child: SafeArea(
            child: Form(
          key: formKey,
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.0.w),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(
                        imgurl,
                      ),
                      radius: 60.r,
                      onBackgroundImageError: (exception, stackTrace) =>
                          const Center(
                              child: Icon(
                        Icons.error,
                        color: Colors.amber,
                      )),
                      backgroundColor: Colors.white,
                    ),
                  )),
                  Expanded(
                    flex: 2,
                    child: SizedBox(
                      height: media.height * 0.061,
                      child: TextFormField(
                        initialValue: imgurl,
                        // validator: (value) {
                        //   if (value!.length < 2) {
                        //     return "too short input";
                        //   }

                        //   return null;
                        // },
                        onChanged: (value) {
                          imgurl = value;
                          print(value);
                          print("###########");
                          print(imgurl);
                          setState(
                            () {},
                          );
                        },
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.start,
                        textAlignVertical: TextAlignVertical.center,
                        decoration: const InputDecoration(
                          hintStyle: TextStyle(color: Colors.white),
                          hintText: "Image Url",
                          alignLabelWithHint: true,
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)),
                          disabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white10)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                flex: 5,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: SingleChildScrollView(
                    child: Column(children: [
                      // CustomInput(
                      //   hint: "Product ID",
                      //   title: "id",
                      // ),
                      SizedBox(
                        height: 20.w,
                      ),
                      CustomInput(
                        number: false,
                        controller: nameController,
                        hint: "Product Name",
                        title: "name",
                      ),
                      SizedBox(
                        height: 20.w,
                      ),
                      CustomInput(
                        number: false,
                        controller: descriptionController,
                        hint: " Prduct Description",
                        title: "description",
                      ),
                      SizedBox(
                        height: 20.w,
                      ),
                      CustomInput(
                        number: false,
                        controller: brandController,
                        hint: " Prduct Brand",
                        title: "brand",
                      ),
                      SizedBox(
                        height: 20.w,
                      ),
                      CustomInput(
                        number: true,
                        controller: priceController,
                        hint: " Prduct Price",
                        title: "Price",
                      ),
                      SizedBox(
                        height: 20.w,
                      ),
                      BlocBuilder<ChooseTypeCubit, String>(
                        builder: (context, state) {
                          if (widget.value != null) {
                            state = widget.value!.type;
                          }

                          return SizedBox(
                            child: DropdownButton(
                              padding: EdgeInsets.zero,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: media.width * 0.05),
                              onChanged: (value) {
                                context
                                    .read<ChooseTypeCubit>()
                                    .chooseType(value!);
                              },
                              value: state,
                              alignment: AlignmentDirectional.centerStart,
                              hint: SizedBox(
                                width: 140.w,
                                child: Text(
                                  'Choose Category',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyLarge!
                                      .copyWith(
                                          color: Colors.white, fontSize: 25.sp),
                                ),
                              ),
                              items: [
                                DropdownMenuItem(
                                  alignment: Alignment.centerLeft,
                                  value: 'electronics',
                                  child: SizedBox(
                                    width: 140.w,
                                    child: Text(
                                      'electronics',
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyLarge!
                                          .copyWith(
                                              color: Colors.white,
                                              fontSize: 18.sp),
                                    ),
                                  ),
                                ),
                                DropdownMenuItem(
                                  alignment: Alignment.centerLeft,
                                  value: 'fur',
                                  child: SizedBox(
                                    width: 140.w,
                                    child: Text('Furniture',
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyLarge!
                                            .copyWith(
                                                color: Colors.white,
                                                fontSize: 18.sp)),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                      SizedBox(
                        height: media.height * 0.01,
                      ),

                      isLoading
                          ? Center(
                              child: CircularProgressIndicator(
                                  color: ColorTheme.secondary),
                            )
                          : Padding(
                              padding: const EdgeInsets.symmetric(vertical: 30),
                              child: InkWell(
                                onTap: () async {
                                  if (formKey.currentState!.validate()) {
                                    if (widget.value != null) {
                                      setState(() {
                                        isLoading = true;
                                      });

                                      context.read<GetAddUpDeleteBloc>().add(
                                          UpdateEvent(
                                              pro: Product(
                                                  id: widget.value!.id,
                                                  img: imgurl,
                                                  pName: nameController.text,
                                                  brand: brandController.text,
                                                  creatorId:
                                                      dependecy<
                                                              SharedPreferences>()
                                                          .getString("userId")!,
                                                  description:
                                                      descriptionController
                                                          .text,
                                                  price: double.parse(
                                                      priceController.text),
                                                  rate: 4.5,
                                                  type: context
                                                      .read<ChooseTypeCubit>()
                                                      .state)));
                                    } else {
                                      setState(() {
                                        isLoading = true;
                                      });
                                      context.read<GetAddUpDeleteBloc>().add(
                                          AddProductEvent(
                                              pro: Product(
                                                  img: imgurl == ''
                                                      ? 'https://img.freepik.com/free-photo/tv-with-wide-screen_144627-12166.jpg?w=740&t=st=1688301109~exp=1688301709~hmac=e2fa8ea6b35329d5e4e71cf64fd422fbfbd507d7de0cd123a541eeb3a1df0e8b'
                                                      : imgurl,
                                                  pName: nameController.text,
                                                  brand: brandController.text,
                                                  creatorId:
                                                      dependecy<
                                                              SharedPreferences>()
                                                          .getString("userId")!,
                                                  description:
                                                      descriptionController
                                                          .text,
                                                  price: double.parse(
                                                      priceController.text),
                                                  rate: 4.5,
                                                  type: context
                                                      .read<ChooseTypeCubit>()
                                                      .state)));
                                    }
                                  }
                                },
                                child: Container(
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(15.r)),
                                    width: media.width * 0.4,
                                    height: media.width * 0.12,
                                    child: Text(
                                      "Send",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyLarge!
                                          .copyWith(
                                              color: ColorTheme.main,
                                              fontSize: 25.sp),
                                    )),
                              ),
                            )
                    ]),
                  ),
                ),
              ),
            ],
          ),
        )),
      ),
    );
  }
}
