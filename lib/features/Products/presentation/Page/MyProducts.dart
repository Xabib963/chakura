import 'package:chakura/InjectionContainer.dart';
import 'package:chakura/features/Order/presentation/page/ConfirmOrder.dart';
import 'package:chakura/features/Order/presentation/page/Orders.dart';
import 'package:chakura/features/Products/Domain/entities/CartProduct.dart';
import 'package:chakura/features/Products/Domain/entities/Product.dart';
import 'package:chakura/features/Products/presentation/Blocs/bloc/get_add_up_delete_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../GetIn/presentaion/cubit/counter_cubit.dart';

import '../../data/DataProviders/LocalDB.dart';
import '../Blocs/bloc/addto_hive_bloc.dart';
import '../Cubit/chooseTypeCubit.dart/cubit/choose_type_cubit.dart';
import '../Widget/Fav&CartWidget.dart';
import 'AddNewProduct.dart';

class MyProducts extends StatelessWidget {
  MyProducts({super.key});
  static const routeName = '/myProducts';
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "M Y P R O D U C T S",
          style: TextStyle(color: Colors.white, fontSize: media.width * 0.06),
        ),
      ),
      body: SafeArea(
          child: BlocBuilder<GetAddUpDeleteBloc, GetAddUpDeleteState>(
        bloc: dependecy<GetAddUpDeleteBloc>()..add(GetUserProducts()),
        builder: (context, state) {
          return state is GetEleLoaded
              ? SizedBox(
                  height: media.height * 0.75,
                  child: RefreshIndicator(
                    onRefresh: () async {
                      context.read<GetAddUpDeleteBloc>()
                        ..add(GetUserProducts());
                    },
                    child: ListView.builder(
                        itemCount: state.products.length,
                        itemBuilder: (context, index) {
                          return Dismissible(
                            background: Container(
                              margin: EdgeInsets.all(15.r),
                              color: Colors.red,
                              child: const Icon(
                                Icons.delete,
                                color: Colors.white,
                              ),
                            ),
                            key: UniqueKey(),
                            direction: DismissDirection.endToStart,
                            onDismissed: (direction) async {
                              if (direction == DismissDirection.endToStart) {
                                context.read<GetAddUpDeleteBloc>().add(
                                    DeleteEvent(id: state.products[index].id!));
                                context
                                    .read<GetAddUpDeleteBloc>()
                                    .add(GetUserProducts());
                              }
                            },
                            child: ListTile(
                                leading: CircleAvatar(
                                    onBackgroundImageError:
                                        (exception, stackTrace) => const Icon(
                                              Icons.error,
                                              color: Colors.white,
                                            ),
                                    backgroundImage: NetworkImage(
                                      state.products[index].img,
                                    )),
                                trailing: IconButton(
                                    onPressed: () {
                                      Navigator.of(context)
                                          .push(MaterialPageRoute(
                                        builder: (context) =>
                                            BlocProvider<ChooseTypeCubit>(
                                          create: (context) =>
                                              dependecy<ChooseTypeCubit>(),
                                          child: AddProductScreen(
                                              title: "U P D A T E",
                                              value: state.products[index]),
                                        ),
                                      ));
                                    },
                                    icon: const Icon(
                                      Icons.edit,
                                      color: Colors.white,
                                    )),
                                subtitle: SizedBox(
                                  width: 50.w,
                                  child: Row(
                                    children: [
                                      Text(
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        '${state.products[index].brand} ||',
                                        style: TextStyle(
                                            color: Color.fromARGB(
                                                255, 133, 133, 133),
                                            fontSize: media.width * 0.04),
                                      ),
                                      SizedBox(
                                        width: 50.w,
                                        child: Text(
                                            maxLines: 1,
                                            overflow: TextOverflow.fade,
                                            '${state.products[index].description!} ',
                                            style: TextStyle(
                                              fontSize: 15.sp,
                                              color: Color.fromARGB(
                                                  255, 133, 133, 133),
                                            )),
                                      )
                                    ],
                                  ),
                                ),
                                title: Text(
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  state.products[index].pName!,
                                  style: TextStyle(
                                    fontSize: 20.sp,
                                    color: Colors.white,
                                  ),
                                )),
                          );
                        }),
                  ))
              : const Center(
                  child: CircularProgressIndicator(),
                );
        },
      )),
    );
  }
}
