import 'package:chakura/core/themes/Thems.dart';
import 'package:chakura/features/Order/presentation/page/ConfirmOrder.dart';
import 'package:chakura/features/Order/presentation/page/Orders.dart';
import 'package:chakura/features/Products/Domain/entities/CartProduct.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../GetIn/presentaion/cubit/counter_cubit.dart';

import '../../data/DataProviders/LocalDB.dart';
import '../Blocs/bloc/addto_hive_bloc.dart';
import '../Widget/Fav&CartWidget.dart';

class Cart extends StatelessWidget {
  Cart({super.key});

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      floatingActionButton: FloatingActionButton(
        backgroundColor: ColorTheme.secondary,
        onPressed: () {
          context.read<AddtoHiveBloc>().getTotal();
          double total = context.read<AddtoHiveBloc>().total;

          Navigator.of(context).pushNamed(ConfirmOrder.routeName, arguments: {
            "ls": context.read<AddtoHiveBloc>().ls,
            'total': total
          });
        },
        child: const Icon(
          Icons.confirmation_number_outlined,
          color: Colors.white,
        ),
      ),
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5),
            child: Text(
              'Cart :',
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontFamily: 'Poppins',
                    color: Colors.white,
                    fontSize: 30.sp,
                    fontWeight: FontWeight.w300,
                  ),
            ),
          ),
          SizedBox(
            height: media.height * 0.75,
            child: BlocBuilder<AddtoHiveBloc, AddtoHiveState>(
              bloc: context.read<AddtoHiveBloc>()
                ..add(GetCartItems(box: LocalDB.instance.getCartsBox())),
              builder: (context, state) {
                if (state is CartItemsLoaded) {
                  return RefreshIndicator(
                    onRefresh: () async {
                      context.read<AddtoHiveBloc>().add(
                          GetCartItems(box: LocalDB.instance.getCartsBox()));
                    },
                    child: ListView.builder(
                        itemCount: state.products.length,
                        itemBuilder: (context, index) {
                          return Dismissible(
                            background: Container(
                              margin: EdgeInsets.all(15),
                              color: Colors.red,
                              child: const Icon(
                                Icons.delete,
                                color: Colors.white,
                              ),
                            ),
                            key: UniqueKey(),
                            direction: DismissDirection.endToStart,
                            onDismissed: (direction) {
                              if (direction == DismissDirection.endToStart) {
                                context.read<AddtoHiveBloc>().add(
                                    RemoveFromCartLocalEvent(
                                        pro: state.products[index],
                                        box: LocalDB.instance.getCartsBox()));
                                Future.delayed(const Duration(seconds: 1));
                                context.read<AddtoHiveBloc>().add(GetCartItems(
                                    box: LocalDB.instance.getCartsBox()));
                              }
                            },
                            child: FavAndCartWidget(
                                isCart: true,
                                cart: state.products[index],
                                media: media),
                          );
                        }),
                  );
                }
                if (state is AddtoHiveInitial) {
                  return const Center(
                    child: CircularProgressIndicator(
                      color: Colors.white,
                    ),
                  );
                }
                return const SizedBox();
              },
            ),
          )
        ],
      )),
    );
  }
}
