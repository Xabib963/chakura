import 'package:chakura/features/Products/presentation/Blocs/bloc/addto_hive_bloc.dart';

import 'package:chakura/features/Products/presentation/Widget/ProductShow.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../../../InjectionContainer.dart';
import '../../../../Widgets/CateguriesItem.dart';

import '../../../../core/themes/Thems.dart';
import '../../../GetIn/presentaion/cubit/categuries_cubit_cubit.dart';
import 'package:swipeable_card_stack/swipeable_card_stack.dart';

import 'EquipmentDiatels.dart';
import '../Blocs/bloc/get_add_up_delete_bloc.dart';

class Equipments extends StatelessWidget {
  Equipments({super.key});

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    return SafeArea(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: media.height * 0.83,
          child: RefreshIndicator(
            onRefresh: () async {
              context.read<GetAddUpDeleteBloc>().add(GetElectronicProEvent());
              dependecy<GetAddUpDeleteBloc>().add(GetFurnitureProEvent());
            },
            child: SingleChildScrollView(
              child: Wrap(
                direction: Axis.vertical,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.0.w, vertical: 5.h),
                    child: Text(
                      'Electronics :',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontFamily: 'Poppins',
                            color: Colors.white,
                            fontSize: 25.sp,
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                  ),
                  SizedBox(
                    height: media.height * 0.48,
                    width: media.width,
                    child: BlocBuilder<GetAddUpDeleteBloc, GetAddUpDeleteState>(
                        bloc: context.read<GetAddUpDeleteBloc>()
                          ..add(GetElectronicProEvent()),
                        builder: (context, state) {
                          if (state is GetEleInitial) {
                            return const Center(
                              child: CircularProgressIndicator(
                                color: Colors.white,
                              ),
                            );
                          }

                          if (state is GetEleLoaded) {
                            return ListView.builder(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemCount: state.products.length,
                                itemBuilder: (context, index) => ProductShow(
                                    product: state.products[index]));
                          }
                          if (state is GetError) {
                            return SizedBox(
                              child: Text(state.messege),
                            );
                          }
                          return SizedBox();
                        }),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.0.w, vertical: 5.w),
                    child: Text(
                      'Furnitures :',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontFamily: 'Poppins',
                            color: Colors.white,
                            fontSize: 25.sp,
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                  ),
                  SizedBox(
                    height: media.height * 0.48,
                    width: media.width,
                    child: BlocBuilder<GetAddUpDeleteBloc, GetAddUpDeleteState>(
                        bloc: dependecy<GetAddUpDeleteBloc>()
                          ..add(GetFurnitureProEvent()),
                        builder: (context, state) {
                          if (state is GetEleInitial) {
                            return const Center(
                              child: CircularProgressIndicator(
                                color: Colors.white,
                              ),
                            );
                          }

                          if (state is GetEleLoaded) {
                            return ListView.builder(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemCount: state.products.length,
                                itemBuilder: (context, index) => ProductShow(
                                    product: state.products[index]));
                          }
                          if (state is GetError) {
                            return Center(
                              child: Text(
                                state.messege,
                                style: textStyle(
                                    size: 20.sp,
                                    color: Colors.white,
                                    weight: FontWeight.bold),
                              ),
                            );
                          }
                          return SizedBox();
                        }),
                  ),
                  SizedBox(
                    height: media.height * 0.12,
                  ),
                  SizedBox(
                    height: media.height * 0.12,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ));
  }
}

// the overFlowed desighn
