import 'package:chakura/features/Products/presentation/Blocs/bloc/addto_hive_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../../../Widgets/CateguriesItem.dart';
import '../../../../main.dart';
import '../../../../core/themes/Thems.dart';
import '../../Domain/entities/CartProduct.dart';
import '../../Domain/entities/Product.dart';
import '../../data/DataProviders/LocalDB.dart';
import '../Blocs/bloc/get_add_up_delete_bloc.dart';

class EquipmentDiatels extends StatefulWidget {
  EquipmentDiatels({super.key});
  static const routeName = '/equiomentDeteils';
  // static route() {
  //   return MaterialPageRoute(
  //     settings: RouteSettings(name: routeName),
  //     builder: (context) => MultiBlocProvider(
  //       providers: [
  //         BlocProvider.value(
  //           value: context.read<AddtoHiveBloc>(),
  //         ),
  //         BlocProvider.value(value: context.read<GetAddUpDeleteBloc>())
  //       ],
  //       child: EquipmentDiatels(),
  //     ),
  //   );
  // }

  @override
  State<EquipmentDiatels> createState() => _EquipmentDiatelsState();
}

class _EquipmentDiatelsState extends State<EquipmentDiatels> {
  @override
  Widget build(BuildContext context) {
    Product pro = ModalRoute.of(context)!.settings.arguments as Product;
    Size media = MediaQuery.of(context).size;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Text(
          'Dieatels',
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w400,
            fontSize: 20.sp,
          ),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: const Icon(
              Icons.arrow_back_ios_new,
              color: Colors.white,
            )),
      ),
      body: SafeArea(
          top: false,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: media.width,
                height: media.height * 0.5,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.vertical(bottom: Radius.circular(25.r)),
                      gradient: LinearGradient(
                          begin: Alignment.bottomCenter,
                          end: Alignment.topLeft,
                          colors: [
                            ColorTheme.mainOpa..withOpacity(0.5),
                            ColorTheme.mainLight..withOpacity(0.8),
                            ColorTheme.main..withOpacity(0.5),
                          ])),
                  child: Card(
                      color: Colors.transparent,
                      margin: EdgeInsets.zero,
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.vertical(
                              bottom: Radius.circular(25))),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Image.network(
                              pro.img,
                              width: media.width * 0.4,
                            ),
                          )
                        ],
                      )),
                ),
              ),
              SizedBox(
                width: media.width * 0.95,
                height: media.height * 0.45,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                width: media.width * 0.7,
                                child: Text(
                                  pro.pName,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 30.sp,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: media.width * 0.25,
                                child: Text(
                                  '${pro.price}\$',
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    color: Colors.green[800],
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20.sp,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                '${pro.type} reviews',
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 133, 133, 133),
                                  fontWeight: FontWeight.w400,
                                  fontSize: 15.sp,
                                ),
                              ),
                              SizedBox(width: media.width * 0.05),
                              SizedBox(
                                width: media.width * 0.25,
                                child: Row(
                                  children: [
                                    const Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                    ),
                                    Text(
                                      '${pro.rate}',
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w300,
                                        fontSize: 18.sp,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 2),
                        child: SizedBox(
                          width: media.width * 0.98,
                          height: media.height * 0.25,
                          child: SingleChildScrollView(
                            child: RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.white70,
                                      fontSize: 18.sp,
                                    ),
                                    children: [
                                  TextSpan(text: pro.description),
                                ])),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          BlocConsumer<AddtoHiveBloc, AddtoHiveState>(
                            listener: (context, state) {
                              if (state is AddtoFavorites ||
                                  state is RemovefromLocal) {
                                context
                                    .read<AddtoHiveBloc>()
                                    .add(CheckEvent(pro));

                                print('yeah');
                              } else {
                                print("khara");
                              }
                            },
                            bloc: context.read<AddtoHiveBloc>()
                              ..add(CheckEvent(pro)),
                            builder: (context, state) {
                              return InkWell(
                                borderRadius: BorderRadius.circular(25.r),
                                onTap: () {
                                  context.read<AddtoHiveBloc>().contain
                                      ? {
                                          context.read<AddtoHiveBloc>()
                                            ..add(RemoveFromFavoritesLocalEvent(
                                                pro: pro,
                                                box: LocalDB.instance
                                                    .getFavoritesBox()))
                                        }
                                      : {
                                          context.read<AddtoHiveBloc>()
                                            ..add(AddtofavoritesEvent(
                                                pro: pro,
                                                box: LocalDB.instance
                                                    .getFavoritesBox()))
                                        };
                                },
                                child: Container(
                                    width: media.width * 0.15,
                                    height: media.width * 0.15,
                                    clipBehavior: Clip.hardEdge,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: ColorTheme.main,
                                      // border: Border.all(
                                      //     width: 1, color: ColorTheme.additional),
                                    ),
                                    child: Center(
                                        child: context
                                                .read<AddtoHiveBloc>()
                                                .contain
                                            ? Icon(
                                                size: 35.sp,
                                                color: Colors.white,
                                                Icons.favorite)
                                            : Icon(
                                                size: 35.sp,
                                                color: Colors.white,
                                                Icons.favorite_border))),
                              );
                            },
                          ),
                          InkWell(
                            onTap: () async {
                              await LocalDB.instance.getCartsBox().put(
                                  pro.id,
                                  CartProduct(
                                      id: pro.id,
                                      img: pro.img,
                                      pName: pro.pName,
                                      brand: pro.brand,
                                      creatorId: pro.creatorId,
                                      totalPrice: pro.price,
                                      quantity: 0,
                                      type: pro.type));
                            },
                            child: Container(
                                width: media.width * 0.65,
                                height: media.width * 0.15,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50),
                                    gradient: LinearGradient(
                                        begin: Alignment.bottomCenter,
                                        end: Alignment.topLeft,
                                        colors: [
                                          ColorTheme.mainLight
                                            ..withOpacity(0.5),
                                          ColorTheme.mainLight
                                            ..withOpacity(0.5),
                                          ColorTheme.additional
                                            ..withOpacity(0.5),
                                        ])),
                                child: Center(
                                    child: Text(
                                  'Add to Cart',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20.sp,
                                  ),
                                ))),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
