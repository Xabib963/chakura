import 'package:chakura/InjectionContainer.dart';
import 'package:chakura/features/Products/Domain/entities/Product.dart';
import 'package:chakura/features/Products/data/DataProviders/LocalDB.dart';
import 'package:chakura/features/Products/presentation/Blocs/bloc/addto_hive_bloc.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../GetIn/presentaion/cubit/add_tofavorites_cubit.dart';
import '../../../GetIn/presentaion/cubit/counter_cubit.dart';
import '../../../../main.dart';
import '../../../../core/themes/Thems.dart';
import '../Widget/Fav&CartWidget.dart';
import 'EquipmentDiatels.dart';

class Favorites extends StatefulWidget {
  Favorites({super.key});

  @override
  State<Favorites> createState() => _FavoritesState();
}

class _FavoritesState extends State<Favorites> {
  bool show = false;

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5),
            child: Text(
              'favorites :',
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontFamily: 'Poppins',
                    color: Colors.white,
                    fontSize: 30.sp,
                    fontWeight: FontWeight.w400,
                  ),
            ),
          ),
          SizedBox(
            height: media.height * 0.75,
            child: BlocBuilder<AddtoHiveBloc, AddtoHiveState>(
              bloc: context.read<AddtoHiveBloc>()
                ..add(GetItems(box: LocalDB.instance.getFavoritesBox())),
              builder: (context, state) {
                if (state is ItemsLoaded) {
                  return RefreshIndicator(
                    onRefresh: () async {
                      context.read<AddtoHiveBloc>().add(
                          GetItems(box: LocalDB.instance.getFavoritesBox()));
                    },
                    child: ListView.builder(
                        itemCount: state.products.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              Navigator.of(context).pushNamed(
                                  EquipmentDiatels.routeName,
                                  arguments: state.products[index]);
                            },
                            child: FavAndCartWidget(
                                isCart: false,
                                pro: state.products[index],
                                media: media),
                          );
                        }),
                  );
                }
                if (state is AddtoHiveInitial) {
                  return const Center(
                    child: CircularProgressIndicator(
                      color: Colors.white,
                    ),
                  );
                }
                return const SizedBox();
              },
            ),
          )
        ],
      )),
    );
  }
}
