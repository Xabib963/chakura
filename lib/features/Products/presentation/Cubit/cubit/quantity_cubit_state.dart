part of 'quantity_cubit_cubit.dart';

abstract class QuantityCubitState extends Equatable {
  const QuantityCubitState();

  @override
  List<Object> get props => [];
}

class QuantityCubitInitial extends QuantityCubitState {}
