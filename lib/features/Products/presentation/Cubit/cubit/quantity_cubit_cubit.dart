import 'package:bloc/bloc.dart';
import 'package:chakura/features/Products/Domain/entities/CartProduct.dart';
import 'package:equatable/equatable.dart';

part 'quantity_cubit_state.dart';

class QuantityCubitCubit extends Cubit<int> {
  QuantityCubitCubit() : super(0);
  updateQuantityAdd(CartProduct product) {
    product.quantity += 1;

    emit(state + 1);
  }

  updateQuantityMines(CartProduct product) {
    if (product.quantity > 0) {
      product.quantity -= 1;
      emit(state - 1);
    }
  }
}
