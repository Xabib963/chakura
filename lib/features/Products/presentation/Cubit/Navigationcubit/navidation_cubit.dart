import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'navidation_state.dart';

class NavidationCubit extends Cubit<NavidationState> {
  NavidationCubit() : super(NavidationInitial());
  void homeLand() => emit(NavidationInitial());
  void favoriteLand() => emit(NavidationFav());
  void cartLand() => emit(NavidationCart());
}
