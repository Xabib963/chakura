part of 'navidation_cubit.dart';

abstract class NavidationState extends Equatable {
  const NavidationState();

  @override
  List<Object> get props => [];
}

class NavidationInitial extends NavidationState {}

class NavidationFav extends NavidationState {}

class NavidationCart extends NavidationState {}
