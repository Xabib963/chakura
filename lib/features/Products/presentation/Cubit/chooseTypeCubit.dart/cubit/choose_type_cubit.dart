import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

class ChooseTypeCubit extends Cubit<String> {
  ChooseTypeCubit() : super('electronics');
  chooseType(String type) {
    emit(type);
  }
}
