import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

part 'choose_image_state.dart';

class ChooseImageCubit extends Cubit<String> {
  ChooseImageCubit() : super('noImage');

  ImagePicker picker = ImagePicker();
  XFile? image;
  // StorageService storageService = StorageService();

  chooseImage(BuildContext context) async {
    image = await picker.pickImage(source: ImageSource.gallery);
    if (image == null) {}
    if (image != null) {
      // await storageService.uploadImage(image);
      // showBottomSheet(context: context, builder: );
      // String imageURl =
      //     await storageService.getDownloadUrl(image!.name);
    }
  }
}
