part of 'choose_image_cubit.dart';

abstract class ChooseImageState extends Equatable {
  const ChooseImageState();

  @override
  List<Object> get props => [];
}

class ChooseImageInitial extends ChooseImageState {}
