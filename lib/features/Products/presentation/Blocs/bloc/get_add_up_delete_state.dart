// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'get_add_up_delete_bloc.dart';

abstract class GetAddUpDeleteState extends Equatable {
  const GetAddUpDeleteState();

  @override
  List<Object> get props => [];
}

class GetEleInitial extends GetAddUpDeleteState {}

class GetEleLoaded extends GetAddUpDeleteState {
  final List<Product> products;
  GetEleLoaded({
    required this.products,
  });
  @override
  List<Object> get props => [products];
}

class GetError extends GetAddUpDeleteState {
  final String messege;

  GetError({required this.messege});

  @override
  List<Object> get props => [messege];
}

class AddUpDeleteInitial extends GetAddUpDeleteState {}

class AddUpDeleteMessegeState extends GetAddUpDeleteState {
  final String messege;

  AddUpDeleteMessegeState({required this.messege});

  @override
  List<Object> get props => [messege];
}

class AddUpDeleteErrorMessegeState extends GetAddUpDeleteState {
  final String messege;

  AddUpDeleteErrorMessegeState({required this.messege});

  @override
  List<Object> get props => [messege];
}
