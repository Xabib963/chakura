// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'addto_hive_bloc.dart';

abstract class AddtoHiveState extends Equatable {
  const AddtoHiveState();

  @override
  List<Object> get props => [];
}

class AddtoHiveInitial extends AddtoHiveState {}

class ItemsLoaded extends AddtoHiveState {
  final List<Product> products;
  ItemsLoaded({
    required this.products,
  });
}

class CartItemsLoaded extends AddtoHiveState {
  final List<CartProduct> products;
  CartItemsLoaded({
    required this.products,
  });
}

class AddtoFavorites extends AddtoHiveState {}

class RemovefromLocal extends AddtoHiveState {}

class AddtoCart extends AddtoHiveState {}

class ErrorAdd extends AddtoHiveState {
  final String messege;
  ErrorAdd({
    required this.messege,
  });
}
