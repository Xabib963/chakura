// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'get_add_up_delete_bloc.dart';

abstract class GetAddUpDeleteEvent extends Equatable {
  const GetAddUpDeleteEvent();

  @override
  List<Object> get props => [];
}

class GetElectronicProEvent extends GetAddUpDeleteEvent {
  const GetElectronicProEvent();

  @override
  List<Object> get props => [];
}

class GetFurnitureProEvent extends GetAddUpDeleteEvent {
  const GetFurnitureProEvent();

  @override
  List<Object> get props => [];
}

class GetGameProEvent extends GetAddUpDeleteEvent {
  const GetGameProEvent();

  @override
  List<Object> get props => [];
}

class AddProductEvent extends GetAddUpDeleteEvent {
  final Product pro;
  const AddProductEvent({required this.pro});

  @override
  List<Object> get props => [pro];
}

class UpdateEvent extends GetAddUpDeleteEvent {
  final Product pro;
  const UpdateEvent({required this.pro});

  @override
  List<Object> get props => [pro];
}

class DeleteEvent extends GetAddUpDeleteEvent {
  final String id;
  const DeleteEvent({required this.id});

  @override
  List<Object> get props => [id];
}

class GetUserProducts extends GetAddUpDeleteEvent {
  const GetUserProducts();

  @override
  List<Object> get props => [];
}
