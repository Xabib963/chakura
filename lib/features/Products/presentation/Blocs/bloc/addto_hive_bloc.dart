// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:bloc/bloc.dart';
import 'package:chakura/features/Products/Domain/entities/CartProduct.dart';
import 'package:chakura/features/Products/Domain/usecases/DeletFromFavorites.dart';
import 'package:chakura/features/Products/Domain/usecases/GetFavoritesUsecase.dart';
import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';

import 'package:chakura/features/Products/Domain/usecases/AddToFvoritesUseCases.dart';

import '../../../../../core/errors/failures.dart';
import '../../../Domain/entities/Product.dart';
import '../../../Domain/usecases/CheckstatusUsecase.dart';
import '../../../Domain/usecases/GetCartProuductsUseCase.dart';
import '../../../data/DataProviders/LocalDB.dart';

part 'addto_hive_event.dart';
part 'addto_hive_state.dart';

class AddtoHiveBloc extends Bloc<AddtoHiveEvent, AddtoHiveState> {
  final AddToFvoritesUseCases addToFvoritesUseCases;
  final CheckStatusUseCase checkStatusUseCase;
  final GetFavoritesItemsUseCase getFavoritesItemsUseCase;
  final GetCartsItemsUseCase getCartsItemsUseCase;
  final Remove_Product_FromLocal remove_product_fromLocal;
  bool contain = false;
  List<CartProduct> ls = [];
  double total = 0;
  getTotal() {
    ls.forEach((e) {
      total += e.totalPrice * e.quantity;
    });
  }

  AddtoHiveBloc(
    this.addToFvoritesUseCases,
    this.checkStatusUseCase,
    this.getFavoritesItemsUseCase,
    this.remove_product_fromLocal,
    this.getCartsItemsUseCase,
  ) : super(AddtoHiveInitial()) {
    on<CheckEvent>((event, emit) async {
      var failureOrDone = await checkStatusUseCase(event.pro);
      failureOrDone
          .fold((l) => emit(ErrorAdd(messege: _mapFailureTomessege(l))), (r) {
        contain = r;
        emit(AddtoHiveInitial());
      });
    });
    on<AddtofavoritesEvent>((event, emit) async {
      var failureOrDone = await addToFvoritesUseCases(
          LocalDB.instance.getFavoritesBox(), event.pro);
      failureOrDone
          .fold((l) => emit(ErrorAdd(messege: _mapFailureTomessege(l))), (r) {
        contain = true;
        emit(AddtoFavorites());
      });
    });
    on<RemoveFromFavoritesLocalEvent>((event, emit) async {
      var failureOrDone =
          await remove_product_fromLocal(event.box, event.pro, null);
      failureOrDone
          .fold((l) => emit(ErrorAdd(messege: _mapFailureTomessege(l))), (r) {
        contain = false;
        emit(RemovefromLocal());
      });
    });
    on<RemoveFromCartLocalEvent>((event, emit) async {
      var failureOrDone =
          await remove_product_fromLocal(event.box, null, event.pro);
      failureOrDone
          .fold((l) => emit(ErrorAdd(messege: _mapFailureTomessege(l))), (r) {
        contain = false;
        emit(RemovefromLocal());
      });
    });
    on<GetItems>((event, emit) async {
      emit(AddtoHiveInitial());
      var failureOrDone = await getFavoritesItemsUseCase(
        event.box,
      );
      failureOrDone
          .fold((l) => emit(ErrorAdd(messege: _mapFailureTomessege(l))), (r) {
        contain = true;
        emit(ItemsLoaded(products: r));
      });
    });
    on<GetCartItems>((event, emit) async {
      emit(AddtoHiveInitial());
      var failureOrDone = await getCartsItemsUseCase(
        event.box,
      );
      failureOrDone
          .fold((l) => emit(ErrorAdd(messege: _mapFailureTomessege(l))), (r) {
        ls = r;
        print(ls);
        contain = true;
        emit(CartItemsLoaded(products: r));
      });
    });
  }
  String _mapFailureTomessege(Failure fail) {
    switch (fail.runtimeType) {
      case ServicesFailure:
        return '';
      case DataSourceFailure:
        return '';
      case OffLineFailure:
        return '';
      default:
        return "UnEcpected Error";
    }
  }
}
