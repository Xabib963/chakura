import 'package:bloc/bloc.dart';
import 'package:chakura/features/Products/Domain/usecases/GetUserProductsUseCase.dart';
import 'package:equatable/equatable.dart';
import '../../../../../core/errors/failures.dart';
import '../../../Domain/entities/Product.dart';
import '../../../Domain/usecases/AddNewProductsUSecase.dart';
import '../../../Domain/usecases/DeleteProductUseCase.dart';
import '../../../Domain/usecases/GetAllproductUseCase.dart';

import '../../../Domain/usecases/GetfurProducts.dart';
import '../../../Domain/usecases/UpdateProductUseCase.dart';

part 'get_add_up_delete_event.dart';
part 'get_add_up_delete_state.dart';

class GetAddUpDeleteBloc
    extends Bloc<GetAddUpDeleteEvent, GetAddUpDeleteState> {
  final GetElectronicsProductUseCase getAllproductUseCase;
  final GetfurProductUseCase getfurProductUseCase;
  final UpdateProductUseCase updateProductUseCase;
  final AddNewProductsUSecase addNewProductsUSecase;
  final GetUserProductsUseCase getUserProducts;
  final DeleteProductUseCase deleteProductUseCase;
  GetAddUpDeleteBloc(
      {required this.deleteProductUseCase,
      required this.getUserProducts,
      required this.getfurProductUseCase,
      required this.addNewProductsUSecase,
      required this.getAllproductUseCase,
      required this.updateProductUseCase})
      : super(GetEleInitial()) {
    on<GetElectronicProEvent>((event, emit) async {
      emit(GetEleInitial());
      var products = await getAllproductUseCase();
      products.fold((l) => emit(GetError(messege: _mapFailureTomessege(l))),
          (r) => emit(GetEleLoaded(products: r)));
    });
    on<GetFurnitureProEvent>((event, emit) async {
      emit(GetEleInitial());
      var products = await getfurProductUseCase();
      products.fold((l) => emit(GetError(messege: _mapFailureTomessege(l))),
          (r) => emit(GetEleLoaded(products: r)));
    });
    on<DeleteEvent>((event, emit) async {
      emit(GetEleInitial());
      var products = await deleteProductUseCase(event.id);
      print(products);
      products.fold((l) => emit(GetError(messege: _mapFailureTomessege(l))),
          (r) => emit(AddUpDeleteMessegeState(messege: "Done")));
    });
    on<GetUserProducts>((event, emit) async {
      emit(GetEleInitial());
      var userProducts = await getUserProducts();
      print(userProducts);
      userProducts.fold((l) => emit(GetError(messege: _mapFailureTomessege(l))),
          (r) => emit(GetEleLoaded(products: r)));
    });
    on<UpdateEvent>((event, emit) async {
      emit(AddUpDeleteInitial());
      var failureOrDone = await updateProductUseCase(event.pro);

      failureOrDone.fold(
          (l) => emit(
              AddUpDeleteErrorMessegeState(messege: _mapFailureTomessege(l))),
          (r) => emit(AddUpDeleteMessegeState(messege: "Done")));
    });
    on<AddProductEvent>((event, emit) async {
      var failureOrDone = await addNewProductsUSecase(event.pro);
      emit(AddUpDeleteInitial());
      failureOrDone.fold((l) {
        return emit(
            AddUpDeleteErrorMessegeState(messege: _mapFailureTomessege(l)));
      }, (r) => emit(AddUpDeleteMessegeState(messege: "Add Done")));
    });
  }
  String _mapFailureTomessege(Failure fail) {
    switch (fail.runtimeType) {
      case ServicesFailure:
        return '';
      case DataSourceFailure:
        return '';
      case OffLineFailure:
        return 'please Check your connection';
      default:
        return "UnEcpected Error";
    }
  }
}
