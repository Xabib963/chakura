part of 'addto_hive_bloc.dart';

abstract class AddtoHiveEvent extends Equatable {
  const AddtoHiveEvent();

  @override
  List<Object> get props => [];
}

class CheckEvent extends AddtoHiveEvent {
  final Product pro;

  CheckEvent(this.pro);
}

class AddtofavoritesEvent extends AddtoHiveEvent {
  final Product pro;
  final Box box;
  AddtofavoritesEvent({
    required this.pro,
    required this.box,
  });
}

class RemoveFromFavoritesLocalEvent extends AddtoHiveEvent {
  final Product pro;
  final Box box;
  RemoveFromFavoritesLocalEvent({
    required this.pro,
    required this.box,
  });
}

class RemoveFromCartLocalEvent extends AddtoHiveEvent {
  final CartProduct pro;
  final Box box;
  RemoveFromCartLocalEvent({
    required this.pro,
    required this.box,
  });
}

class GetItems extends AddtoHiveEvent {
  final Box box;
  GetItems({
    required this.box,
  });
}

class GetCartItems extends AddtoHiveEvent {
  final Box box;
  GetCartItems({
    required this.box,
  });
}

class AddtoCartEvent extends AddtoHiveEvent {
  final Product pro;
  final Box box;
  AddtoCartEvent({
    required this.pro,
    required this.box,
  });
}
