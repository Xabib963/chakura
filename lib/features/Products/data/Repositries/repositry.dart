// ignore_for_file: non_constant_identifier_names

import 'dart:developer';

import 'package:chakura/features/Products/Domain/entities/CartProduct.dart';
import 'package:dartz/dartz.dart';
import 'package:hive/hive.dart';

import '../../../../core/Network/connection_info.dart';
import '../../../../core/errors/Exception.dart';
import '../../../../core/errors/failures.dart';
import '../../Domain/entities/Product.dart';
import '../../Domain/repositries/ProductRepositry.dart';
import '../DataProviders/LocalDB.dart';
import '../DataProviders/ProductsDataSource.dart';
import '../Model/ProductModel.dart';

class ProductRepositryImpl implements ProductsRepositry {
  final NetworkInfo networkInfo;
  final ProductREmoteDataSource remoteDataSource;
  final LocalDB localDB;

  ProductRepositryImpl(
      {required this.localDB,
      required this.networkInfo,
      required this.remoteDataSource});

  @override
  Future<Either<Failure, List<Product>>> get_electronics_products() async {
    if (await networkInfo.isConnected) {
      try {
        final remoteProducts = await remoteDataSource.get_electonics_Product();
        return Right(remoteProducts);
      } on ServiceException {
        return Left(ServicesFailure());
      }
    } else {
      // if i had a local data i need to try get products if null throw error
      return Left(OffLineFailure());
    }
  }

  // ignore: non_constant_identifier_names
  @override
  Future<Either<Failure, Unit>> add_Product(Product pro) async {
    if (await networkInfo.isConnected) {
      try {
        await remoteDataSource.add_Product(pro);
        return Right(unit);
      } on ServiceException {
        return Left(ServicesFailure());
      }
    } else {
      return Left(OffLineFailure());
    }
  }

  @override
  Future<Either<Failure, Unit>> delete_product(String? id) async {
    if (await networkInfo.isConnected) {
      try {
        await remoteDataSource.delete_Product(id!);
        return Right(unit);
      } on ServiceException {
        return Left(ServicesFailure());
      }
    } else {
      // if i had a local data i need to try get products if null throw error
      return Left(OffLineFailure());
    }
    return _getMesseg(() => remoteDataSource.delete_Product(id!));
  }

  @override
  Future<Either<Failure, Unit>> updateProduct(Product? pro) async {
    if (await networkInfo.isConnected) {
      try {
        await remoteDataSource.update_Product(pro!);
        return Right(unit);
      } on ServiceException {
        return Left(ServicesFailure());
      }
    } else {
      // if i had a local data i need to try get products if null throw error
      return Left(OffLineFailure());
    }
    return _getMesseg(() => remoteDataSource.update_Product(pro!));
  }

  @override
  Future<Either<Failure, Unit>> add_Product_toFavorite(
      Box box, Product? pro) async {
    try {
      localDB.addProduct(box, pro!);
      return Right(unit);
    } catch (e) {
      return Left(OffLineFailure());
    }
  }

  Future<Either<Failure, Unit>> _getMesseg(
      Future<Unit> Function() dOrUpd) async {
    if (await networkInfo.isConnected) {
      try {
        await dOrUpd;
        return Right(unit);
      } on ServiceException {
        return Left(ServicesFailure());
      }
    } else {
      // if i had a local data i need to try get products if null throw error
      return Left(OffLineFailure());
    }
  }

//  @override
//   Future<Either<Failure, Unit>> remove_Product_FromCart(
//       Box box, Product? pro) async {
//     try {
//       localDB.removeProduct(box, pro!);
//       return Right(unit);
//     } catch (e) {
//       return Left(OffLineFailure());
//     }
//     throw UnimplementedError();
//   }
  @override
  Future<Either<Failure, Unit>> remove_Product_FromLocal(
      Box box, Product? pro, CartProduct? pro2) async {
    try {
      if (pro == null) {
        localDB.removeProduct(box, null, pro2);
      } else {
        localDB.removeProduct(box, pro, null);
      }

      return Right(unit);
    } catch (e) {
      return Left(OffLineFailure());
    }
  }

  @override
  Future<Either<Failure, List<Product>>> get_Games_products() async {
    if (await networkInfo.isConnected) {
      try {
        final remoteProducts = await remoteDataSource.get_electonics_Product();
        return Right(remoteProducts);
      } on ServiceException {
        return Left(ServicesFailure());
      }
    } else {
      // if i had a local data i need to try get products if null throw error
      return Left(OffLineFailure());
    }
  }

  @override
  Future<Either<Failure, List<Product>>> get_fur_products() async {
    if (await networkInfo.isConnected) {
      try {
        final remoteProducts = await remoteDataSource.get_fur_Product();
        return Right(remoteProducts);
      } on ServiceException {
        return Left(ServicesFailure());
      }
    } else {
      // if i had a local data i need to try get products if null throw error
      return Left(OffLineFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> check_status(id) async {
    try {
      bool val = await localDB.getFavoritesFavoritesStatus(id);
      return Right(val);
    } catch (e) {
      return Left(OffLineFailure());
    }
  }

  @override
  Future<Either<Failure, List<Product>>> get_all_favorites_products(
      Box box) async {
    try {
      final favoritesProducts = await localDB.getFavoritesProducts(box);
      return Right(favoritesProducts);
    } on ServiceException {
      return Left(ServicesFailure());
    }
  }

  @override
  Future<Either<Failure, List<CartProduct>>> get_all_carts_products(
      Box box) async {
    try {
      final cartProducts = await localDB.getCartProducts(box);
      return Right(cartProducts);
    } on ServiceException {
      return Left(ServicesFailure());
    }
  }

  @override
  Future<Either<Failure, List<Product>>> get_User_products() async {
    try {
      final userProducts = await remoteDataSource.get_User_Product();
      return Right(userProducts);
    } on ServiceException {
      return Left(ServicesFailure());
    }
    throw UnimplementedError();
  }
}
