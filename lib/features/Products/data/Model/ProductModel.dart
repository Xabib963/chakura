import '../../Domain/entities/Product.dart';

class ProductModel extends Product {
  ProductModel(
      {super.id,
      required super.pName,
      required super.brand,
      required super.creatorId,
      required super.description,
      required super.price,
      required super.rate,
      required super.type,
      required super.img});
  factory ProductModel.fromJson(Map<String, dynamic> json, String id) {
    return ProductModel(
        id: id,
        brand: json["brand"],
        creatorId: json['creatorId'],
        description: json['description'],
        pName: json['pName'],
        price: json['price'],
        rate: json['rate'],
        type: json['type'],
        img: json['img']);
  }

  toJson() {
    return {
      "brand": brand,
      'creatorId': creatorId,
      'description': description,
      'pName': pName,
      'price': price,
      'rate': rate,
      'type': type,
      'img': img
    };
  }
}
