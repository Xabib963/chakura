import 'dart:convert';

import 'package:chakura/InjectionContainer.dart';
import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/Constants.dart';
import '../../../../core/errors/Exception.dart';
import '../../Domain/entities/Product.dart';
import '../Model/ProductModel.dart';

abstract class ProductREmoteDataSource {
  Future<List<Product>> get_electonics_Product();
  Future<List<Product>> get_User_Product();
  Future<List<Product>> get_fur_Product();
  Future<List<Product>> get_Games_Product();
  Future<Unit> delete_Product(String id);
  Future<Unit> update_Product(Product pro);
  Future<Unit> add_Product(Product pro);
}

class RemoteDataWithHttp implements ProductREmoteDataSource {
  String uID = dependecy<SharedPreferences>().getString('userId')!;
  @override
  Future<Unit> add_Product(Product pro) async {
    print(
        '##########################################################saddsasda500');
    var body = jsonEncode({
      "pName": pro.pName,
      "brand": pro.brand,
      "creatorId": pro.creatorId,
      "description": pro.description,
      "price": pro.price,
      "rate": pro.rate,
      "type": pro.type,
      "img": pro.img
    });

    try {
      var response =
          await http.post(Uri.parse('$baseUrl/Products.json'), body: body);
      return unit;
    } catch (e) {
      throw ServiceException();
    }
  }

  @override
  Future<Unit> delete_Product(String id) async {
    try {
      var response = await http.delete(
        Uri.parse(baseUrl + '/Products/${id}.json?'),
      );
      print(response.body);
      return unit;
    } catch (e) {
      throw ServiceException();
    }
  }

  @override
  Future<List<Product>> get_electonics_Product() async {
    try {
      var response = await http.get(Uri.parse(
          '$baseUrl/Products.json?orderBy="type"&equalTo="electronics"'));

      List<ProductModel> products = [];
      (json.decode(response.body) as Map<String, dynamic>)?.forEach(
        (key, value) {
          products.add(ProductModel.fromJson(value, key));
        },
      );

      return products;
    } catch (e) {
      throw ServiceException();
    }
  }

  @override
  Future<List<Product>> get_User_Product() async {
    print(uID);
    try {
      var response = await http.get(Uri.parse(
          '$baseUrl/Products.json?orderBy="creatorId"&equalTo="$uID"'));
      print(response.body);
      List<ProductModel> products = [];
      (json.decode(response.body) as Map<String, dynamic>)?.forEach(
        (key, value) {
          products.add(ProductModel.fromJson(value, key));
        },
      );

      return products;
    } catch (e) {
      throw ServiceException();
    }
  }

  @override
  Future<Unit> update_Product(Product pro) async {
    print(pro);
    var body = jsonEncode({
      "pName": pro.pName,
      "brand": pro.brand,
      "creatorId": pro.creatorId,
      "description": pro.description,
      "price": pro.price,
      "rate": pro.rate,
      "type": pro.type,
      "img": pro.img
    });
    try {
      var response = await http
          .patch(Uri.parse('$baseUrl/Products/${pro.id}.json?'), body: body);
      return unit;
    } catch (e) {
      throw ServiceException();
    }
  }

  @override
  Future<List<Product>> get_Games_Product() async {
    try {
      var response = await http.get(
          Uri.parse('$baseUrl/Products.json?orderBy="type"&equalTo="Games"'));

      List<ProductModel> products = [];
      (json.decode(response.body) as Map<String, dynamic>)?.forEach(
        (key, value) {
          products.add(ProductModel.fromJson(value, key));
        },
      );

      return products;
    } catch (e) {
      print(e);
      throw ServiceException();
    }
  }

  @override
  Future<List<Product>> get_fur_Product() async {
    try {
      var response = await http.get(
          Uri.parse('$baseUrl/Products.json?orderBy="type"&equalTo="fur"'));
      List<ProductModel> products = [];
      (json.decode(response.body) as Map<String, dynamic>)?.forEach(
        (key, value) {
          products.add(ProductModel.fromJson(value, key));
        },
      );

      return products;
    } catch (e) {
      print(e);
      throw ServiceException();
    }
    throw UnimplementedError();
  }
}
