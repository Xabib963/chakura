import 'package:chakura/features/Products/Domain/entities/CartProduct.dart';
import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';

import 'package:path/path.dart';

import '../../Domain/entities/Product.dart';
import '../Model/ProductModel.dart';

class LocalDB {
  static LocalDB instance = LocalDB();
  Future<Box> openFavoritesBox() async {
    Box box = await Hive.openBox<Product>('Favorites');
    return box;
  }

  Future<Box> opencartsBox() async {
    Box box = await Hive.openBox<CartProduct>('Carts');

    return box;
  }

  Box getFavoritesBox() => Hive.box<Product>('Favorites');
  bool getFavoritesFavoritesStatus(dynamic id) =>
      Hive.box<Product>('Favorites').get(id) != null ? true : false;
  Box getCartsBox() => Hive.box<CartProduct>('Carts');

  List<Product> getFavoritesProducts(Box box) {
    return box.values.toList() as List<Product>;
  }

  List<CartProduct> getCartProducts(Box box) {
    return box.values.toList() as List<CartProduct>;
  }

  removeProduct(Box box, Product? product, CartProduct? pro2) async {
    if (product == null) {
      print("object");
      await box.delete(pro2!.id);
    } else {
      await box.delete(product!.id);
    }
  }

  void addProduct(Box box, Product product) async {
    if (box.name == "Carts") {
      await box.put(
          product.id,
          CartProduct(
              img: product.img,
              pName: product.pName,
              brand: product.brand,
              creatorId: product.creatorId,
              totalPrice: product.price,
              quantity: 0,
              type: product.type));
    } else {
      await box.put(product.id, product);
    }
  }
}

// class FavoritesDB implements LocalDB {
//   static FavoritesDB instance = FavoritesDB();
//   static const String boxName = 'favorites';

//   @override
//   openBox() async {
//     Box box = await Hive.openBox<Product>(boxName);
//     return box;
//   }

//   @override
//   Box getBox() => Hive.box<Product>(boxName);
//   @override
//   List<Product> getProducts(Box box) {
//     return box.values.toList() as List<Product>;
//   }

//   @override
//   removeProduct(Box box, Product product) async {
//     await box.delete(product.id);
//   }

//   @override
//   addProduct(Box box, Product product) async {
//     await box.put(product.id, product);
//   }
// }

// class CartsDB implements LocalDB {
//   static CartsDB instance = CartsDB();
//   static const String boxName = 'carts';

//   @override
//   openBox() async {
//     Box box = await Hive.openBox<Product>(boxName);
//     return box;
//   }

//   @override
//   Box getBox() => Hive.box<Product>(boxName);
//   @override
//   List<Product> getProducts(Box box) {
//     return box.values.toList() as List<Product>;
//   }

//   @override
//   removeProduct(Box box, Product product) async {
//     await box.delete(product.id);
//   }

//   @override
//   addProduct(Box box, Product product) async {
//     await box.put(product.id, product);
//   }
// }
