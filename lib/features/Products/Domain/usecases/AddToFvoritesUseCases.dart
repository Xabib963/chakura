import 'package:dartz/dartz.dart';
import 'package:hive/hive.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Product.dart';
import '../repositries/ProductRepositry.dart';

class AddToFvoritesUseCases {
  final ProductsRepositry productsRepositry;

  AddToFvoritesUseCases(this.productsRepositry);

  Future<Either<Failure, Unit>> call(Box box, Product? pro) {
    return productsRepositry.add_Product_toFavorite(box, pro);
  }
}
