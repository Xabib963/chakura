import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Product.dart';
import '../repositries/ProductRepositry.dart';

class DeleteProductUseCase {
  final ProductsRepositry _productsRepositry;

  DeleteProductUseCase(this._productsRepositry);
  Future<Either<Failure, Unit>> call(String id) {
    return _productsRepositry.delete_product(id);
  }
}
