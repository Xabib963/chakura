import 'package:dartz/dartz.dart';
import 'package:hive/hive.dart';

import '../../../../core/errors/failures.dart';
import '../entities/CartProduct.dart';
import '../entities/Product.dart';
import '../repositries/ProductRepositry.dart';

class GetCartsItemsUseCase {
  final ProductsRepositry _productsRepositry;

  GetCartsItemsUseCase(this._productsRepositry);
  Future<Either<Failure, List<CartProduct>>> call(Box box) {
    return _productsRepositry.get_all_carts_products(box);
  }
}
