import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Product.dart';
import '../repositries/ProductRepositry.dart';

class AddNewProductsUSecase {
  final ProductsRepositry _productsRepositry;

  AddNewProductsUSecase(this._productsRepositry);
  Future<Either<Failure, Unit>> call(Product pro) {
    return _productsRepositry.add_Product(pro);
  }
}
