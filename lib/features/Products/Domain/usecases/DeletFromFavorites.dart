import 'package:dartz/dartz.dart';
import 'package:hive/hive.dart';

import '../../../../core/errors/failures.dart';
import '../entities/CartProduct.dart';
import '../entities/Product.dart';
import '../repositries/ProductRepositry.dart';

class Remove_Product_FromLocal {
  final ProductsRepositry productsRepositry;

  Remove_Product_FromLocal(this.productsRepositry);
  Future<Either<Failure, Unit>> call(Box box, Product? pro, CartProduct? pro2) {
    if (pro == null) {
      return productsRepositry.remove_Product_FromLocal(box, null, pro2!);
    } else {
      return productsRepositry.remove_Product_FromLocal(box, pro, null);
    }
  }
}
