import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Product.dart';
import '../repositries/ProductRepositry.dart';

class GetElectronicsProductUseCase {
  final ProductsRepositry _productsRepositry;

  GetElectronicsProductUseCase(this._productsRepositry);
  Future<Either<Failure, List<Product>>> call() {
    return _productsRepositry.get_electronics_products();
  }
}
