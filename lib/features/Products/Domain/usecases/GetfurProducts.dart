import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Product.dart';
import '../repositries/ProductRepositry.dart';

class GetfurProductUseCase {
  final ProductsRepositry _productsRepositry;

  GetfurProductUseCase(this._productsRepositry);
  Future<Either<Failure, List<Product>>> call() {
    return _productsRepositry.get_fur_products();
  }
}
