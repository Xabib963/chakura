import 'package:dartz/dartz.dart';
import 'package:hive/hive.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Product.dart';
import '../repositries/ProductRepositry.dart';

class GetFavoritesItemsUseCase {
  final ProductsRepositry _productsRepositry;

  GetFavoritesItemsUseCase(this._productsRepositry);
  Future<Either<Failure, List<Product>>> call(Box box) {
    return _productsRepositry.get_all_favorites_products(box);
  }
}
