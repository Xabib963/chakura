import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Product.dart';
import '../repositries/ProductRepositry.dart';

class UpdateProductUseCase {
  final ProductsRepositry _productsRepositry;

  UpdateProductUseCase(this._productsRepositry);
  Future<Either<Failure, Unit>> call(Product pro) {
    return _productsRepositry.updateProduct(pro);
  }
}
