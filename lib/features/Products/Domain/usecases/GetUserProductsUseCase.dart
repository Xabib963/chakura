import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';

import '../entities/Product.dart';
import '../repositries/ProductRepositry.dart';

class GetUserProductsUseCase {
  final ProductsRepositry _productsRepositry;

  GetUserProductsUseCase(this._productsRepositry);
  Future<Either<Failure, List<Product>>> call() {
    return _productsRepositry.get_User_products();
  }
}
