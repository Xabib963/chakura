import 'package:dartz/dartz.dart';
import 'package:hive/hive.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Product.dart';
import '../repositries/ProductRepositry.dart';

class CheckStatusUseCase {
  final ProductsRepositry productsRepositry;

  CheckStatusUseCase(this.productsRepositry);

  Future<Either<Failure, bool>> call(Product? pro) {
    return productsRepositry.check_status(pro!.id!);
  }
}
