import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Product.dart';
import '../repositries/ProductRepositry.dart';

class GetgamesProductUseCase {
  final ProductsRepositry _productsRepositry;

  GetgamesProductUseCase(this._productsRepositry);
  Future<Either<Failure, List<Product>>> call() {
    return _productsRepositry.get_Games_products();
  }
}
