import 'package:dartz/dartz.dart';
import 'package:hive/hive.dart';

import '../../../../core/errors/failures.dart';
import '../entities/CartProduct.dart';
import '../entities/Product.dart';
import '../entities/loyaltyOffers.dart';

abstract class ProductsRepositry {
  Future<Either<Failure, List<Product>>> get_User_products();
  Future<Either<Failure, List<Product>>> get_electronics_products();
  Future<Either<Failure, List<Product>>> get_Games_products();
  Future<Either<Failure, List<Product>>> get_fur_products();
  Future<Either<Failure, List<Product>>> get_all_favorites_products(Box box);
  Future<Either<Failure, List<CartProduct>>> get_all_carts_products(Box box);
  Future<Either<Failure, Unit>> updateProduct(Product? pro);
  Future<Either<Failure, Unit>> delete_product(String? id);
  Future<Either<Failure, Unit>> add_Product(Product pro);
  Future<Either<Failure, Unit>> add_Product_toFavorite(Box box, Product? pro);
  Future<Either<Failure, Unit>> remove_Product_FromLocal(
      Box box, Product? pro, CartProduct? pro2);

  Future<Either<Failure, bool>> check_status(dynamic? id);
}
