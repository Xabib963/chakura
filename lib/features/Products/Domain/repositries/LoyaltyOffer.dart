import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Offers.dart';
import '../entities/Product.dart';
import '../entities/loyaltyOffers.dart';

abstract class LoyaltyOffersRepositry {
  Future<Either<Failure, List<LoyaltyOffers>>> get_all_LoyaltyOffers();
  Future<Either<Failure, Unit>> updateLoyaltyOffers(LoyaltyOffers? offer);
  Future<Either<Failure, Unit>> delete_LoyaltyOffers(String? id);
  Future<Either<Failure, Unit>> add_LoyaltyOffers(LoyaltyOffers? offer);
}
