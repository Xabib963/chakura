import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../entities/Offers.dart';
import '../entities/Product.dart';
import '../entities/loyaltyOffers.dart';

abstract class OffersRepositry {
  Future<Either<Failure, List<Offers>>> get_all_Offers();

  Future<Either<Failure, Unit>> updateOffers(Offers? offer);
  Future<Either<Failure, Unit>> delete_Offer(String? id);
  Future<Either<Failure, Unit>> add_Offers(Offers? offer);
}
