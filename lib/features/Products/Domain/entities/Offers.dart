import 'package:equatable/equatable.dart';

class Offers extends Equatable {
  final String des;
  final String proId;

  Offers({required this.des, required this.proId});

  @override
  // TODO: implement props
  List<Object?> get props => [des, proId];
}
