import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
part "Product.g.dart";

@HiveType(typeId: 0)
class Product extends HiveObject {
  @HiveField(0)
  final String? id;
  @HiveField(1)
  final String pName;
  @HiveField(2)
  final String brand;
  @HiveField(3)
  final String creatorId;
  @HiveField(4)
  final String description;
  @HiveField(5)
  final double price;
  @HiveField(6)
  final double rate;
  @HiveField(7)
  final String type;
  @HiveField(8)
  final String img;
  // i used copy because i cant store the same instanc in to diffrent box
  Product get copy {
    Product copy = Product(
        img: this.img,
        pName: this.pName,
        brand: this.brand,
        creatorId: this.creatorId,
        description: this.description,
        price: this.price,
        rate: this.rate,
        type: this.type,
        id: this.id);
    return copy;
  }

  Product(
      {this.id,
      required this.img,
      required this.pName,
      required this.brand,
      required this.creatorId,
      required this.description,
      required this.price,
      required this.rate,
      required this.type});
}
