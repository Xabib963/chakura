import 'package:equatable/equatable.dart';

class LoyaltyOffers extends Equatable {
  final String description;
  final List<String> photos;

  LoyaltyOffers({required this.description, required this.photos});

  @override
  // TODO: implement props
  List<Object?> get props => [description, photos];
}
