import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
part "CartProduct.g.dart";

@HiveType(typeId: 1)
class CartProduct extends HiveObject {
  @HiveField(0)
  final String? id;
  @HiveField(1)
  final String pName;
  @HiveField(2)
  final String brand;
  @HiveField(3)
  final String creatorId;
  @HiveField(4)
  final double totalPrice;
  @HiveField(5)
  final String type;
  @HiveField(6)
  final String img;
  @HiveField(7)
  int quantity;
  // i used copy because i cant store the same instanc in to diffrent box

  CartProduct get copy {
    CartProduct copy = CartProduct(
        img: this.img,
        pName: this.pName,
        brand: this.brand,
        creatorId: this.creatorId,
        totalPrice: this.totalPrice,
        quantity: this.quantity,
        type: this.type,
        id: this.id);
    return copy;
  }

  CartProduct(
      {this.id,
      required this.img,
      required this.pName,
      required this.brand,
      required this.creatorId,
      required this.totalPrice,
      required this.quantity,
      required this.type});
}
