// import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';

// import 'Widgets/CateguriesItem.dart';

// class MyHomePage extends StatefulWidget {
//   const MyHomePage({super.key});

//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   int _bottomNavIndex = 0;
//   @override
//   Widget build(BuildContext context) {
//     var media = MediaQuery.of(context).size;
//     return SafeArea(
//       child: Column(
//         mainAxisSize: MainAxisSize.max,
//         mainAxisAlignment: MainAxisAlignment.start,
//         crossAxisAlignment: CrossAxisAlignment.stretch,
//         children: [
//           // const Mainheader(),
//           // const Searchbar(),
//           SizedBox(
//             height: media.height * 0.15,
//             width: media.width * 0.2,
//             child: CateiguriesItem(),
//           ),
//           Container(
//             height: media.height * 0.32,
//             decoration: const BoxDecoration(
//               color: Colors.transparent,
//             ),
//             child: Padding(
//               padding: const EdgeInsetsDirectional.fromSTEB(5, 0, 5, 0),
//               child: ListView(
//                 padding: EdgeInsets.zero,
//                 shrinkWrap: true,
//                 scrollDirection: Axis.horizontal,
//                 children: [
//                   Material(
//                     color: Colors.transparent,
//                     elevation: 5,
//                     shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(15),
//                     ),
//                     child: Container(
//                       width: media.width * 0.4,
//                       height: media.height * 0.3,
//                       decoration: BoxDecoration(
//                         gradient: const LinearGradient(
//                           colors: [Color(0xFFEE1111), Color(0xFFFB414B)],
//                           stops: [0, 1],
//                           begin: AlignmentDirectional(0, -1),
//                           end: AlignmentDirectional(0, 1),
//                         ),
//                         borderRadius: BorderRadius.circular(15),
//                       ),
//                       child: Card(
//                         clipBehavior: Clip.antiAliasWithSaveLayer,
//                         color: Color(0x02EE1111),
//                         elevation: 0,
//                         shape: RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(15),
//                         ),
//                         child: Flexible(
//                           child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             mainAxisSize: MainAxisSize.max,
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               Container(
//                                 width: media.width * 0.4,
//                                 height: media.height * 0.2,
//                                 decoration: BoxDecoration(
//                                   color: Colors.transparent,
//                                   image: DecorationImage(
//                                     fit: BoxFit.cover,
//                                     image: Image.asset(
//                                       'assets/2022_10_25_05_41_IMG_4910.JPG',
//                                     ).image,
//                                   ),
//                                   borderRadius: const BorderRadius.only(
//                                     bottomLeft: Radius.circular(10),
//                                     bottomRight: Radius.circular(50),
//                                     topLeft: Radius.circular(10),
//                                     topRight: Radius.circular(10),
//                                   ),
//                                 ),
//                               ),
//                               Text(
//                                 overflow: TextOverflow.ellipsis,
//                                 'World war hero',
//                                 textAlign: TextAlign.start,
//                                 maxLines: 1,
//                                 style: Theme.of(context)
//                                     .textTheme
//                                     .headline1!
//                                     .copyWith(
//                                       fontFamily: 'Poppins',
//                                       color: Colors.white,
//                                       fontSize: 16,
//                                     ),
//                               ),
//                               Flexible(
//                                 child: Row(
//                                   mainAxisSize: MainAxisSize.max,
//                                   mainAxisAlignment:
//                                       MainAxisAlignment.spaceBetween,
//                                   children: [
//                                     Padding(
//                                       padding: const EdgeInsets.symmetric(
//                                           vertical: 5, horizontal: 10),
//                                       child: Text(
//                                         '55\$',
//                                         style: Theme.of(context)
//                                             .textTheme
//                                             .bodyText1!
//                                             .copyWith(
//                                               fontFamily: 'Poppins',
//                                               color: Colors.white,
//                                             ),
//                                       ),
//                                     ),
//                                     Padding(
//                                       padding:
//                                           const EdgeInsetsDirectional.fromSTEB(
//                                               0, 0, 5, 5),
//                                       child: Material(
//                                         color: Colors.transparent,
//                                         elevation: 5,
//                                         shape: RoundedRectangleBorder(
//                                           borderRadius:
//                                               BorderRadius.circular(20),
//                                         ),
//                                         child: Container(
//                                           width: 70.8,
//                                           height: 32.8,
//                                           decoration: BoxDecoration(
//                                             color: Colors.white,
//                                             borderRadius:
//                                                 BorderRadius.circular(20),
//                                             shape: BoxShape.rectangle,
//                                           ),
//                                           child: Center(
//                                             child: Text(
//                                               'install',
//                                               style: Theme.of(context)
//                                                   .textTheme
//                                                   .bodyText1!
//                                                   .copyWith(
//                                                     fontFamily: 'Poppins',
//                                                     color: Color(0xFFEE1111),
//                                                     fontSize: 15,
//                                                   ),
//                                             ),
//                                           ),
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
