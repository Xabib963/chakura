import 'dart:async';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:lottie/lottie.dart';

import 'features/Products/presentation/Widget/CustombottomBar.dart';

import 'core/themes/Thems.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});
  static const routeName = '/';
  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          top: false,
          child: Container(
            color: ColorTheme.main,
            width: double.infinity,
            height: media.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Lottie.asset('animation/78811-gaming.json',
                    width: media.width * 0.95, height: media.height * 0.4),
                AnimatedTextKit(
                  animatedTexts: [
                    ColorizeAnimatedText("Chakra",
                        textStyle: TextStyle(
                            fontSize: 40.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[350]),
                        colors: [
                          ColorTheme.additional2,
                          ColorTheme.additional2,
                          ColorTheme.main,
                          ColorTheme.mainOpa
                        ]),
                  ],
                  pause: const Duration(milliseconds: 1000),
                ),
                AnimatedTextKit(animatedTexts: [
                  TypewriterAnimatedText(
                    'The Land of Gamers',
                    textStyle: TextStyle(
                        fontSize: 25.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey[350]),
                    speed: const Duration(milliseconds: 300),
                  ),
                ])
              ],
            ),
          )),
    );
  }
}
